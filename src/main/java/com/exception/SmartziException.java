package com.exception;


public class SmartziException extends RuntimeException {

	   /** The Constant serialVersionUID. */
 private static final long serialVersionUID = 1L;
 
 /** The code. */
 protected int code;
 
 /** The message. */
 protected String message;

 /**
  * Instantiates a new here exception.
  *
  * @param cause cause
  * @param code code
  */
 public SmartziException(Throwable cause, int code) {
     super(cause);
     this.code = code;
 }

 /**
  * Instantiates a new here exception.
  *
  * @param message message
  * @param cause cause
  * @param code code
  */
 public SmartziException(String message, Throwable cause, int code) {
     super(message, cause);
     this.code = code;
 }

 /**
  * Instantiates a new here exception.
  *
  * @param message message
  */
 public SmartziException(String message) {
      super(message);
 }

 /**
  * Instantiates a new here exception.
  *
  * @param code code
  */
 public SmartziException(int code) {
     this.code = code;
 }

 /**
  * Gets the code.
  *
  * @return int code
  */
 public int getCode() {
     return code;
 }

 /**
  * Sets the code.
  *
  * @param code code
  */
 public void setCode(int code) {
     this.code = code;
 }

 /**
  * Instantiates a new here exception.
  *
  * @param message message
  * @param code code
  */
 public SmartziException(String message, int code) {
     super(message);
     this.code = code;
 }
}
