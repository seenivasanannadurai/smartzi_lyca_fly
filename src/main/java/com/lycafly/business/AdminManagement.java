package com.lycafly.business;

import java.util.List;

import com.lycafly.dto.request.BookRideRequest;
import com.smartzi.dto.response.BookRideResponse;
import com.smartzi.dto.response.TripFareResponse;

public interface AdminManagement {

	/**
	 * get vehicle types supported by smartzi
	 *
	 * 
	 */
	public List<TripFareResponse> getVehicleTypes();

	/**
	 * To Assign job to driver
	 *
	 * 
	 */
	public BookRideResponse bookRide(BookRideRequest bookRideRequest);


}
