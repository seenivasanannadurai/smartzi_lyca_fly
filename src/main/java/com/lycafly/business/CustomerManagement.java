package com.lycafly.business;

import com.lycafly.dto.request.RecommendedDrivers;
import com.lycafly.entity.CustomerDetailEntity;
import com.smartzi.dto.request.GetPreferDriverRequest;
import com.smartzi.dto.response.CustomerEmailsByMobNoResp;
import com.smartzi.dto.response.PreferredDriverListEntity;

import java.util.List;

;

public interface CustomerManagement {

	public List<CustomerEmailsByMobNoResp> searchCustomerDetail(String emailId, String mobileNo);

	public CustomerDetailEntity registerCustomer(String name, String mobileNo, String emailId);

	public List<RecommendedDrivers> getPreferDriverList(Long customerId);
}
