package com.lycafly.business;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.client.RestTemplate;

public interface RestCallManagement {


	String callWebservice(Object object, String requestUrl, boolean isAuthRequired);

}
