package com.lycafly.business;

import com.lycafly.dto.request.CancelRideRequest;
import com.lycafly.dto.request.RideRequest;
import com.lycafly.dto.response.CancelRideResponse;
import com.lycafly.dto.response.CreateRideResponse;
import com.lycafly.dto.response.RideStatusResponse;
import com.smartzi.dto.request.UpdateRideRequest;

public interface TripManagement {
	
	/**
	 * Business logic to respond offer requestOfferResponse
	 *
	 * @param hereRideRequest
	 *            the here ride request
	 * @return HereRidesResponse
	 */
	CreateRideResponse CreateRide(RideRequest hereRideRequest);

	/**
	 * Business logic to cancelride
	 *
	 * @param cancelRise
	 * @return CancelRideResponse
	 */
	void cancelRide(CancelRideRequest cancelRise);

	/**
	 * Business logic to get ride status
	 *
	 * @param rideId
	 * @return rideId
	 */
	RideStatusResponse getRideStatus(Long rideId);
	
	/**
	 * Business logic to update ride
	 *
	 * @param updateRideRequest
	 *            the here ride request
	 * @return CreateRideResponse
	 */
	void updateRide(UpdateRideRequest updateRideRequest);


}
