package com.lycafly.business.impl;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.lycafly.business.AdminManagement;
import com.lycafly.business.RestCallManagement;
import com.lycafly.dto.request.BookRideRequest;
import com.smartzi.dto.request.AssignjobtoDriver;
import com.smartzi.dto.response.BookRideResponse;
import com.smartzi.dto.response.TripFareResponse;
import com.smartzi.dto.response.UpdatePaymentModeResponse;

@Service
public class AdminManagementImpl implements AdminManagement {

	@Autowired
	RestCallManagement restCallMagement;
	@Value("${smartzi.vehicle.types}")
	private String vehicleTypeUrl;

	@Value("${smartzi.admin.assignjob}")
	private String assignJobUrl;

	@Override
	public List<TripFareResponse> getVehicleTypes() {
		List<TripFareResponse> tripFareResponseList = null;
		try {
			JsonNode actualObj = null;
			String response = restCallMagement.callWebservice(null, vehicleTypeUrl, false);
			actualObj = new ObjectMapper().readTree(response);
			tripFareResponseList = new ObjectMapper().readValue(actualObj.get("data").toString(),
					new TypeReference<List<TripFareResponse>>() {
					});
		}

		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return tripFareResponseList;
	}

	@Override
	public BookRideResponse bookRide(BookRideRequest bookRideRequest) {
		BookRideResponse bookRideResponse = new BookRideResponse();
		try {
			JsonNode actualObj = null;
			UpdatePaymentModeResponse updatePaymentModeResponse = new UpdatePaymentModeResponse();
			AssignjobtoDriver assignjobtoDriver = new AssignjobtoDriver();
			assignjobtoDriver.setDriverId(bookRideRequest.getDriverId());
			assignjobtoDriver.setTripRequestId(bookRideRequest.getRequestId());
			assignjobtoDriver.setIsMarkePrice(true);
			assignjobtoDriver.setControllerBookingTrip(false);
			assignjobtoDriver.setIsAssign("true");
			assignjobtoDriver.setReferralCode("LYCA-FLY");
			String response = restCallMagement.callWebservice(assignjobtoDriver, assignJobUrl, true);
			actualObj = new ObjectMapper().readTree(response);
			updatePaymentModeResponse = new ObjectMapper().readValue(actualObj.get("data").toString(),
					UpdatePaymentModeResponse.class);
			bookRideResponse.setRideId(updatePaymentModeResponse.getTripReservationId());
			bookRideResponse.setBookReference(updatePaymentModeResponse.getBookingId());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return bookRideResponse;
	}

}
