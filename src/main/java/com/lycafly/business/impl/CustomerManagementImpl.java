package com.lycafly.business.impl;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.lycafly.business.CustomerManagement;
import com.lycafly.business.RestCallManagement;
import com.lycafly.dto.request.RecommendedDrivers;
import com.lycafly.entity.CustomerDetailEntity;
import com.lycafly.repository.TripsRepository;
import com.smartzi.dto.request.CustomerEmailsByMobNoReq;
import com.smartzi.dto.request.DeviceDetailDTO;
import com.smartzi.dto.request.GetPreferDriverRequest;
import com.smartzi.dto.request.UserRegistrationRequest;
import com.smartzi.dto.response.CustomerEmailsByMobNoResp;
import com.smartzi.dto.response.CustomerRegistrationResponse;
import com.smartzi.dto.response.PreferredDriverListEntity;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;

@Service
public class CustomerManagementImpl implements CustomerManagement {

	@Autowired
	TripsRepository tripsRepository;
	@Autowired
	RestCallManagement restCallMaagement;
	@Value("${smartzi.admin.customer.search}")
	private String customerSearch;
	@Value("${smartzi.pereferdriver.list}")
	private String preferDriverListUrl;
	@Value("${smartzi.admin.customer.register}")
	private String customerRegistrationUrl;

	public CustomerDetailEntity registerCustomer(String name, String mobileNo, String emailId) {
		CustomerRegistrationResponse customerRegistrationResponse = new CustomerRegistrationResponse();
		CustomerDetailEntity customerDetailEntity = new CustomerDetailEntity();

		try {
			JsonNode actualObj = null;
			UserRegistrationRequest userRegistrationRequest = new UserRegistrationRequest();
			customerDetailEntity.setCountryCode("+44");
			customerDetailEntity.setEmailId(emailId);
			customerDetailEntity.setFirstName(name);
			DeviceDetailDTO deviceDetail = new DeviceDetailDTO();
			userRegistrationRequest.setFirstName(name);
			userRegistrationRequest.setLastName(name);
			userRegistrationRequest.setEmailId(emailId);
			userRegistrationRequest.setCountryCode("+44");
			userRegistrationRequest.setUserType((short) 3);
			userRegistrationRequest.setMobileNo(mobileNo);
			userRegistrationRequest.setPassword("ea4e17f6e7fe55fad6e60c9e1448091809c41e54d94d336c8d3be98fa938fb71");
			userRegistrationRequest.setReferralCode("TEST001");
			userRegistrationRequest.setUserId(null);
			deviceDetail.setAppType((short) 2);
			deviceDetail.setAppPlatform((short) 2);
			deviceDetail.setDeviceModel("samsung");
			deviceDetail.setDeviceName("user mobile");
			deviceDetail.setDeviceToken("'ea4e17f6e7fe55fad6e60c9e1448091809c41e54d94d336c8d3be98fa938fb71");
			deviceDetail.setAppPlatformVersion("10.3.3");
			deviceDetail.setDeviceLocale("en");
			userRegistrationRequest.setDeviceDetail(deviceDetail);
			String response = restCallMaagement.callWebservice(userRegistrationRequest, customerRegistrationUrl, false);
			actualObj = new ObjectMapper().readTree(response);
			customerRegistrationResponse = new ObjectMapper().readValue(actualObj.get("data").toString(),
					CustomerRegistrationResponse.class);
			customerDetailEntity.setCustomerId(customerRegistrationResponse.getCustomerId());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return customerDetailEntity;

	}

	public List<CustomerEmailsByMobNoResp> searchCustomerDetail(String mobileNo, String firstName) {
		List<CustomerEmailsByMobNoResp> customerEmailsByMobNoRespList = null;
		try {
			JsonNode actualObj = null;
			CustomerEmailsByMobNoReq customerEmailsByMobNoReq = new CustomerEmailsByMobNoReq();
			customerEmailsByMobNoReq.setMobileNo(mobileNo);
			System.out.println("CustomerRegistrationResponse***********" + mobileNo);
			String response = restCallMaagement.callWebservice(customerEmailsByMobNoReq, customerSearch, true);
			System.out.println("CustomerRegistrationResponse***********" + response);
			actualObj = new ObjectMapper().readTree(response);
			customerEmailsByMobNoRespList = new ObjectMapper().readValue(actualObj.get("data").toString(),
					new TypeReference<List<CustomerEmailsByMobNoResp>>() {
					});
			customerEmailsByMobNoRespList.stream().filter(userDetail -> userDetail.getMobileNo().equals(mobileNo)
					&& userDetail.getFirstName().equals(firstName));

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return customerEmailsByMobNoRespList;
	}

	@Override
	public List<RecommendedDrivers> getPreferDriverList(Long customerId) {
		List<RecommendedDrivers> preferDriverList = null;
		try {
			JsonNode actualObj = null;
			GetPreferDriverRequest preferDriverRequest=new GetPreferDriverRequest();
			preferDriverRequest.setCustomerId(customerId);
			String response = restCallMaagement.callWebservice(preferDriverRequest, preferDriverListUrl, true);
			System.out.println("prefre response"+response);
			actualObj = new ObjectMapper().readTree(response);
			preferDriverList = new ObjectMapper().readValue(actualObj.get("data").toString(),
					new TypeReference<List<RecommendedDrivers>>() {
					});
		} catch (IOException e) {

		}

		// TODO Auto-generated method stub
		return preferDriverList;
	}
}
