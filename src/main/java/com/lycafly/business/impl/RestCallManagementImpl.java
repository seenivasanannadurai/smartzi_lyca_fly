package com.lycafly.business.impl;

import com.lycafly.business.RestCallManagement;
import com.smrtzi.token.TokenApi;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class RestCallManagementImpl implements RestCallManagement {
	private final Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	RestTemplate restTemplate;

	@Autowired
	TokenApi tokenApi;
	@Value("${smartzi.base.url}")
	private String baseUrl;

	public String callWebservice(Object object, String requestUrl, boolean isAuthRequired) {

		String response = null;
		try {
			if (isAuthRequired) {
				HttpHeaders headers = new HttpHeaders();
				headers.setContentType(MediaType.APPLICATION_JSON);
				headers.set("Authorization", "Bearer " + tokenApi.getApiToken());
				HttpEntity<Object> request = new HttpEntity<Object>(object, headers);
				response = restTemplate.postForEntity(baseUrl + requestUrl, request, String.class).getBody();
			} else {
				response = restTemplate.postForEntity(baseUrl + requestUrl, object, String.class).getBody();

			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return response;
	}
}
