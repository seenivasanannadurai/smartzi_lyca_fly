package com.lycafly.business.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.lycafly.business.CustomerManagement;
import com.lycafly.business.RestCallManagement;
import com.lycafly.business.TripManagement;
import com.lycafly.constants.SupplierRideStatusRideStatus;
import com.lycafly.dto.request.CancelRideRequest;
import com.lycafly.dto.request.RecommendedDrivers;
import com.lycafly.dto.request.RideRequest;
import com.lycafly.dto.response.CancelRideResponse;
import com.lycafly.dto.response.CreateRideResponse;
import com.lycafly.dto.response.DirectionAPIResponse;
import com.lycafly.dto.response.EstimatedRideDurationSeconds;
import com.lycafly.dto.response.RideStatusResponse;
import com.lycafly.dto.response.SmartziResponse;
import com.lycafly.entity.CancelRideEntity;
import com.lycafly.entity.CreateRideEntity;
import com.lycafly.entity.CustomerDetailEntity;
import com.lycafly.entity.RideDetailsEntity;
import com.lycafly.entity.RideStatusEntity;
import com.lycafly.repository.TripsRepository;
import com.lycafly.utils.BookingSourceEnum;
import com.lycafly.utils.BookingSourceType;
import com.lycafly.utils.DateUtil;
import com.lycafly.utils.MapUtil;
import com.smartzi.dto.request.AddressDetails;
import com.smartzi.dto.request.GetTripReservDetailRequest;
import com.smartzi.dto.request.TaxiBookingRequest;
import com.smartzi.dto.request.TripEnquiryRequest;
import com.smartzi.dto.request.UpdateRideRequest;
import com.smartzi.dto.request.UpdateTripReservStatusRequest;
import com.smartzi.dto.response.CustomerEmailsByMobNoResp;
import com.smartzi.dto.response.PreferredDriverListEntity;
import com.smartzi.dto.response.TripEnquiryResponse;
import com.smartzi.dto.response.TripReservationEntity;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class TripManagementImpl implements TripManagement {
	@Autowired
	TripsRepository tripsRepository;
	@Autowired
	RestCallManagement restCallMagement;
	@Autowired
	CustomerManagement customerManagement;
	@Value("${smartzi.trip.request}")
	private String tripRequestUrl;

	@Value("${smartzi.admin.markert.price}")
	private String marketPriceUrl;

	@Value("${smartzi.direction.api.key}")
	private String mapKey;

	@Value("${smartzi.admin.customer.register}")
	private String customerRegistrationUrl;

	@Value("${smartzi.cancel.trip}")
	private String tripCancelUrl;

	@Value("${smartzi.trip.reservation.detail}")
	private String tripReservationUrl;

	@Value("${smartzi.admin.update.tripdetails}")
	private String updateTripUrl;

	@Override
	public CreateRideResponse CreateRide(RideRequest rideRequest) {

		CreateRideResponse createRideResponse = new CreateRideResponse();
		EstimatedRideDurationSeconds estimatedRideDurationSeconds = new EstimatedRideDurationSeconds();
		try {
			List<RecommendedDrivers> recommenderDriverList = new ArrayList<>();
			TripEnquiryResponse tripEnquiryResponse = new TripEnquiryResponse();
			CustomerDetailEntity customerDetailEntity = new CustomerDetailEntity();
			CreateRideEntity createRideEntity = new CreateRideEntity();
			RideDetailsEntity rideDetail = new RideDetailsEntity();
			String mobileNo = rideRequest.getPassenger().getPhoneNumber();
			String name = rideRequest.getPassenger().getName();
			String emailId = rideRequest.getPassenger().getEmail();
			ObjectWriter objectWriter = new ObjectMapper().writer().withDefaultPrettyPrinter();
			createRideEntity.setRideRequest(objectWriter.writeValueAsString(rideRequest));
			createRideEntity.setrideResponse(objectWriter.writeValueAsString(createRideResponse));
			createRideEntity.setsupplier_id(rideRequest.getSupplierId());
			List<CustomerEmailsByMobNoResp> customerDetailList = customerManagement.searchCustomerDetail(mobileNo,
					name);
			if (customerDetailList.size() > 0) {
				customerDetailEntity.setMobileNo(mobileNo);
				customerDetailEntity.setFirstName(name);
				customerDetailEntity.setEmailId(customerDetailList.get(0).getEmailId());
				customerDetailEntity.setCustomerId(customerDetailList.get(0).getCustomerId());
				tripEnquiryResponse = sendTripRequest(rideRequest, customerDetailEntity);
				recommenderDriverList = customerManagement
						.getPreferDriverList(customerDetailList.get(0).getCustomerId());
			} else {
				customerDetailEntity = customerManagement.registerCustomer(name, mobileNo, emailId);
				tripEnquiryResponse = sendTripRequest(rideRequest, customerDetailEntity);
			}
			rideDetail.setrequest_id(tripEnquiryResponse.getTripRequestId());
			rideDetail.setReservationId(tripEnquiryResponse.getTripReservationId());
			rideDetail.setsupplier_id(rideRequest.getSupplierId());
			createRideResponse.setRequestId(tripEnquiryResponse.getTripRequestId());
			estimatedRideDurationSeconds.setValue(50);
			if (recommenderDriverList.size() > 0) {
				createRideResponse.setRecommendedDrivers(recommenderDriverList);

			}
			// tripsRepository.insertIntoRideDetails(rideDetail);
			tripsRepository.insertIntoRideRequest(createRideEntity);
		}
		// TODO Auto-generated method stub
		catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return createRideResponse;
	}

	@Override
	public void cancelRide(CancelRideRequest cancelRide) {
		CancelRideResponse cancelRideResponse = new CancelRideResponse();
		ObjectWriter objectWriter = new ObjectMapper().writer().withDefaultPrettyPrinter();
		UpdateTripReservStatusRequest updateTripReservStatusRequest = new UpdateTripReservStatusRequest();
		CancelRideEntity cancelRideEntity = new CancelRideEntity();
		try {
			// RideDetailsEntity rideEntity =
			// tripsRepository.getRideDetails(cancelRide.getRideId());
			updateTripReservStatusRequest.setTripReservationStatus(3);
			updateTripReservStatusRequest.setCancelReason(cancelRide.getCancelReason());
			updateTripReservStatusRequest.setTripReservationId(cancelRide.getRideId());
			;
			String response = restCallMagement.callWebservice(updateTripReservStatusRequest, tripCancelUrl, true);
			cancelRideResponse.setcancel_accepted("true");
			cancelRideResponse.setcancel_reason(cancelRide.getCancelReason());
			// cancelRideEntity.setRideId(cancelRide.getRideId());
			cancelRideEntity.setCancelReason(cancelRide.getCancelReason());
			cancelRideEntity.setRequest(objectWriter.writeValueAsString(cancelRide));
			cancelRideEntity.setResponse(objectWriter.writeValueAsString(cancelRideResponse));
			// tripsRepository.insertCancelRideRequest(cancelRideEntity);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public RideStatusResponse getRideStatus(Long rideId) {
		RideStatusResponse rideStatusResponse = new RideStatusResponse();
		ObjectWriter objectWriter = new ObjectMapper().writer().withDefaultPrettyPrinter();
		RideStatusEntity rideStatusEntity = new RideStatusEntity();
		try {
			JsonNode actualObj = null;
			GetTripReservDetailRequest getTripReservDetailRequest = new GetTripReservDetailRequest();
			// RideDetailsEntity rideEntity =
			// tripsRepository.getRideDetails(rideId);
			// getTripReservDetailRequest.setTripReservationId((long) 1242);
			TripReservationEntity tripReservationEntity = new TripReservationEntity();
			getTripReservDetailRequest.setTripReservationId(rideId);
			String response = restCallMagement.callWebservice(getTripReservDetailRequest, tripReservationUrl, true);
			actualObj = new ObjectMapper().readTree(response);
			tripReservationEntity = new ObjectMapper().readValue(actualObj.get("data").toString(),
					TripReservationEntity.class);
			rideStatusResponse.setStatus(getStatus(tripReservationEntity));
			rideStatusResponse.setRideId(rideId);
			// rideStatusResponse.setRide_id(rideId);
			// rideStatusEntity.setRide_id(rideId);
			rideStatusEntity.setRideStatusResponse(objectWriter.writeValueAsString(rideStatusResponse));
			// tripsRepository.inserRideStatusRequest(rideStatusEntity);
		} catch (IOException e) {
			rideStatusEntity.setException(e.toString());
			tripsRepository.inserRideStatusRequest(rideStatusEntity);
			e.printStackTrace();
		}

		return rideStatusResponse;
	}

	public String getStatus(TripReservationEntity tripReservationEntity) {

		String statusCode = null;
		int tripStatus = 0;
		Optional<TripReservationEntity> tripReservationObj = Optional.ofNullable(tripReservationEntity);
		if (tripReservationObj.isPresent()) {
			if (Optional.ofNullable(tripReservationEntity.getTripStatus()).orElse(0) != 0) {
				tripStatus = tripReservationEntity.getTripStatus();
				switch (tripStatus) {
				case 1:
					statusCode = SupplierRideStatusRideStatus.AT_PICKUP;
					break;
				case 2:
					statusCode = SupplierRideStatusRideStatus.DRIVER_EN_ROUTE;
					break;
				case 3:
					statusCode = SupplierRideStatusRideStatus.DRIVER_EN_ROUTE;
					break;
				case 4:
					statusCode = SupplierRideStatusRideStatus.COMPLETED;
					break;
				case 5:
					statusCode = SupplierRideStatusRideStatus.CANCELED;
					break;
				case 6:
					statusCode = SupplierRideStatusRideStatus.CANCELED;
					break;
				default:
					statusCode = SupplierRideStatusRideStatus.UNKNOWN;
					break;
				}
			} else {
				switch (tripReservationEntity.getTripReservationStatus()) {
				case 1:
					statusCode = SupplierRideStatusRideStatus.UNKNOWN;
					break;
				case 2:
					statusCode = SupplierRideStatusRideStatus.DRIVER_ASSIGNED;
					break;
				case 3:
					statusCode = SupplierRideStatusRideStatus.CANCELED;
					break;
				case 4:
					statusCode = SupplierRideStatusRideStatus.REJECTED;
					break;
				default:
					statusCode = SupplierRideStatusRideStatus.UNKNOWN;
					break;
				}
			}
		}

		return statusCode;
	}

	public TripEnquiryResponse sendTripRequest(RideRequest rideRequest, CustomerDetailEntity customerDetailEntity) {
		TripEnquiryResponse tripEnquiryResponse = new TripEnquiryResponse();
		try {
			TripEnquiryRequest tripEnquiryRequest = new TripEnquiryRequest();
			TaxiBookingRequest taxiBookingRequest = new TaxiBookingRequest();
			taxiBookingRequest.setCountryCode("+44");
			taxiBookingRequest.setEmailId(customerDetailEntity.getEmailId());
			taxiBookingRequest.setFirstName(customerDetailEntity.getFirstName());
			taxiBookingRequest.setLastName(customerDetailEntity.getFirstName());
			taxiBookingRequest.setMobileNo(customerDetailEntity.getMobileNo());
			Double pickupLat = rideRequest.getPickup().getPoint().getLat();
			Double pickupLang = rideRequest.getPickup().getPoint().getLng();
			Double destinationLat = rideRequest.getDestination().getPoint().getLat();
			Double destinationLang = rideRequest.getDestination().getPoint().getLng();
			String sourcepostCode = rideRequest.getPickup().getAddress().getPostalCode();
			String destPostCode = rideRequest.getDestination().getAddress().getPostalCode();
			AddressDetails addressDetail = getAddress(rideRequest);
			// DirectionAPIResponse directionAPIResponse =
			// MapUtil.getDistance(pickupLat, pickupLang, destinationLat,
			// destinationLang, mapKey);
			
			  String convertUtcTime = DateUtil.convertTimeStampToDate(
			  rideRequest.getPickupTime().getSecondsSinceEpoch());
			 
			//String convertUtcTime = "25-03-2019 10:06";

			tripEnquiryRequest.setCurrentLocationLat(rideRequest.getPickup().getPoint().getLat());
			tripEnquiryRequest.setCurrentLocationLong(rideRequest.getPickup().getPoint().getLng());
			tripEnquiryRequest.setCurrentLocationAlt(0.0);
			tripEnquiryRequest.setCustomerId(customerDetailEntity.getCustomerId());
			tripEnquiryRequest.setSourceAddress(addressDetail.getSourceAddress());
			tripEnquiryRequest.setSourceLat(rideRequest.getPickup().getPoint().getLat());
			tripEnquiryRequest.setSourceLong(rideRequest.getPickup().getPoint().getLng());
			tripEnquiryRequest.setDestinationAddress(addressDetail.getDestinationAddress());
			tripEnquiryRequest.setDestinationLat(rideRequest.getDestination().getPoint().getLat());
			tripEnquiryRequest.setDestinationLong(rideRequest.getDestination().getPoint().getLng());
			tripEnquiryRequest.setTripReqDateTime(rideRequest.getPickupTime().getSecondsSinceEpoch());
			tripEnquiryRequest.setTravelDateTime(convertUtcTime);
			tripEnquiryRequest.setSourceTimeZone("Europe/London");
			tripEnquiryRequest.setDistance(20.0);
			tripEnquiryRequest.setEstimatedTime("40");
			tripEnquiryRequest.setUtcTravelDateTime(DateUtil.getDateTimeForTimezone(convertUtcTime, "Europe/London"));
			tripEnquiryRequest.setVehicleType(rideRequest.getVehicleType());
			tripEnquiryRequest.setReferralCode("LYCA-0001");
			tripEnquiryRequest.setAdminNotes(rideRequest.getPassengerNote());
			tripEnquiryRequest.setBookingSource(BookingSourceEnum.OUTSOURCE.getId());
			tripEnquiryRequest.setBookingSourceType(BookingSourceType.OUTSOURCE.getId());
			tripEnquiryRequest.setBookingSelect(BookingSourceType.OUTSOURCE.getId());
			tripEnquiryRequest.setDestPostCode(sourcepostCode);
			tripEnquiryRequest.setDestPostCode(destPostCode);
			tripEnquiryRequest.setAppPlatFormID(3);
			tripEnquiryRequest.setPaymentTypeId((short) 5);
			taxiBookingRequest.setTripEnquiryRequestTariff(tripEnquiryRequest);
			String response = restCallMagement.callWebservice(taxiBookingRequest, tripRequestUrl, true);

			JsonNode actualObj = null;
			actualObj = new ObjectMapper().readTree(response);
			tripEnquiryResponse = new ObjectMapper().readValue(actualObj.get("data").toString(),
					TripEnquiryResponse.class);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return tripEnquiryResponse;
	}

	public AddressDetails getAddress(RideRequest rideRequest)

	{
		AddressDetails addressDetail = new AddressDetails();

		String sourceAddress = rideRequest.getPickup().getAddress().getStreet() + ","
				+ rideRequest.getPickup().getAddress().getDistrict() + ","
				+ rideRequest.getPickup().getAddress().getState() + ","
				+ rideRequest.getPickup().getAddress().getCountry() + ","
				+ rideRequest.getPickup().getAddress().getPostalCode();

		String destinationAddress = rideRequest.getDestination().getAddress().getStreet() + ","
				+ rideRequest.getDestination().getAddress().getDistrict() + ","
				+ rideRequest.getDestination().getAddress().getState() + ","
				+ rideRequest.getDestination().getAddress().getCountry() + ","
				+ rideRequest.getDestination().getAddress().getPostalCode();

		addressDetail.setSourceAddress(sourceAddress);
		addressDetail.setDestinationAddress(destinationAddress);

		return addressDetail;
	}

	@Override
	public void updateRide(UpdateRideRequest updateRideRequest) {
		try {
			JsonNode actualObj = null;
			GetTripReservDetailRequest getTripReservDetailRequest = new GetTripReservDetailRequest();
			TripReservationEntity tripReservationEntity = new TripReservationEntity();
			getTripReservDetailRequest.setTripReservationId(updateRideRequest.getRideId());
			String response = restCallMagement.callWebservice(getTripReservDetailRequest, tripReservationUrl, true);
			actualObj = new ObjectMapper().readTree(response);
			tripReservationEntity = new ObjectMapper().readValue(actualObj.get("data").toString(),
					TripReservationEntity.class);
			String responseUpdateTrip = restCallMagement.callWebservice(
					getTripReservationentity(tripReservationEntity, updateRideRequest), updateTripUrl, true);

		} catch (IOException e) {
		}

	}

	public TripReservationEntity getTripReservationentity(TripReservationEntity tripReservationEntity,
			UpdateRideRequest updateRideRequest) {

		String sourceAddress = updateRideRequest.getPickup().getAddress().getStreet() + ","
				+ updateRideRequest.getPickup().getAddress().getDistrict() + ","
				+ updateRideRequest.getPickup().getAddress().getState() + ","
				+ updateRideRequest.getPickup().getAddress().getCountry() + ","
				+ updateRideRequest.getPickup().getAddress().getPostalCode();

		String destinationAddress = updateRideRequest.getDestination().getAddress().getStreet() + ","
				+ updateRideRequest.getDestination().getAddress().getDistrict() + ","
				+ updateRideRequest.getDestination().getAddress().getState() + ","
				+ updateRideRequest.getDestination().getAddress().getCountry() + ","
				+ updateRideRequest.getDestination().getAddress().getPostalCode();

		String travelDatetime = DateUtil
				.convertTimeStampToDate(updateRideRequest.getPickupTime().getSecondsSinceEpoch());

		Double pickupLat = updateRideRequest.getPickup().getPoint().getLat();
		Double pickupLang = updateRideRequest.getPickup().getPoint().getLng();
		Double destinationLat = updateRideRequest.getDestination().getPoint().getLat();
		Double destinationLang = updateRideRequest.getDestination().getPoint().getLng();

		tripReservationEntity.setSourceLat(updateRideRequest.getPickup().getPoint().getLat());
		tripReservationEntity.setSourceLong(updateRideRequest.getPickup().getPoint().getLng());
		tripReservationEntity.setDestinationLong(updateRideRequest.getDestination().getPoint().getLat());
		tripReservationEntity.setDestinationLat(updateRideRequest.getDestination().getPoint().getLng());
		tripReservationEntity.setSourceAddress(sourceAddress);
		tripReservationEntity.setDestinationAddress(destinationAddress);
		tripReservationEntity.setTravelDateTime(travelDatetime);
		tripReservationEntity.setUtcTravelDateTime(travelDatetime);
		// DirectionAPIResponse directionAPIResponse =
		// MapUtil.getDistance(pickupLat, pickupLang, destinationLat,
		// destinationLang, mapKey);
		tripReservationEntity.setDistance(20.0);
		tripReservationEntity.setEstimatedTime("40");
		return tripReservationEntity;
	}
}
