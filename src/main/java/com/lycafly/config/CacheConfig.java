package com.lycafly.config;

import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.concurrent.ConcurrentMapCache;
import org.springframework.cache.support.SimpleCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Arrays;

// TODO: Auto-generated Javadoc

/**
 * The Class CacheConfig.
 */
@Configuration
@EnableCaching
public class CacheConfig {

    /**
     * Cache manager.
     *
     * @return the cache manager
     */
    @Bean
    public CacheManager cacheManager() {
        SimpleCacheManager cacheManager = new SimpleCacheManager();
        cacheManager.setCaches(Arrays.asList(
                new ConcurrentMapCache("tokenCache")));
        return cacheManager;
    }
}