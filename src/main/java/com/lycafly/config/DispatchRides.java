package com.lycafly.config;

import org.springframework.stereotype.Service;

// TODO: Auto-generated Javadoc

/**
 * The Class DispatchRides.
 */
@Service
public class DispatchRides {
	
	/** The offers. */
	private String offers;	
	
	/** The dispatcher offer id. */
	private DispatcherOffer dispatcher_offer_id;

	/**
	 * Gets the dispatcher offer id.
	 *
	 * @return the dispatcher offer id
	 */
	public DispatcherOffer getDispatcher_offer_id() {
		return dispatcher_offer_id;
	}

	/**
	 * Sets the dispatcher offer id.
	 *
	 * @param dispatcher_offer_id the new dispatcher offer id
	 */
	public void setDispatcher_offer_id(DispatcherOffer dispatcher_offer_id) {
		this.dispatcher_offer_id = dispatcher_offer_id;
	}
	

	/**
	 * Gets the offers.
	 *
	 * @return the offers
	 */
	public String getOffers() {
		return offers;
	}

	/**
	 * Sets the offers.
	 *
	 * @param offers the new offers
	 */
	public void setOffers(String offers) {
		this.offers = offers;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "DispatchRides [offers=" + offers + ", dispatcher_offer_id=" + dispatcher_offer_id + "]";
	}

	
	

}
