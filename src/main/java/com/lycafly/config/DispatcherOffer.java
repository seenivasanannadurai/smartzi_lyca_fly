package com.lycafly.config;

import org.springframework.stereotype.Service;

// TODO: Auto-generated Javadoc

/**
 * The Class DispatcherOffer.
 */
@Service
public class DispatcherOffer {

	/** The dispatcher offer id. */
	private String dispatcher_offer_id;

	/**
	 * Gets the dispatcher offer id.
	 *
	 * @return the dispatcher offer id
	 */
	public String getDispatcher_offer_id() {
		return dispatcher_offer_id;
	}

	/**
	 * Sets the dispatcher offer id.
	 *
	 * @param dispatcher_offer_id the new dispatcher offer id
	 */
	public void setDispatcher_offer_id(String dispatcher_offer_id) {
		this.dispatcher_offer_id = dispatcher_offer_id;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "DispatcherOffer [dispatcher_offer_id=" + dispatcher_offer_id + "]";
	}
}
