package com.lycafly.constants;


public class ApplicationConstant {
	  
  	/** The Constant SUCCESS_STATUS. */
    public static final String SUCCESS_STATUS = "1000";
    
    /** The Constant INTERNAL_SERVER_ERROR. */
    public static final String INTERNAL_SERVER_ERROR = "1036";
    
    /** The Constant AUDIT_LOGGER. */
    public static final String AUDIT_LOGGER = "AUDIT_LOGGER";
    
    /** The Constant START_TIME. */
    public static final String START_TIME = "00:00";
    
    /** The Constant END_TIME. */
    public static final String END_TIME = "23:59";
    
    /** The Constant ZERO. */
    public static final Integer ZERO = 0;
    
    /** The Constant MASKING_SYMBOL. */
    public static final String MASKING_SYMBOL = "X";
    
    /** The Constant NUMBER_OF_DIGITS_TO_DISPLAY. */
    public static final Integer NUMBER_OF_DIGITS_TO_DISPLAY = 4;
    public static final String SUCCESS = "success";
    /**
     * The Interface RequestStatus.
     */
    public interface RequestStatus {
        
        /** The received. */
        Byte RECEIVED = 1;
        
        /** The processed. */
        Byte PROCESSED = 2;
        
        /** The failed. */
        Byte FAILED = 3;
    }



    
  
  
}
