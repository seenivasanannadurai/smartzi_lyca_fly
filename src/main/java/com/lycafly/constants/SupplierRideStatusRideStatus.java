package com.lycafly.constants;

public class SupplierRideStatusRideStatus {

	public static final String UNKNOWN="UNKNOWN";
	public static final String REJECTED="REJECTED";
	public static final String ACCEPTED="ACCEPTED";
	public static final String DRIVER_ASSIGNED="DRIVER_ASSIGNED";
	public static final String DRIVER_EN_ROUTE="DRIVER_EN_ROUTE";
	public static final String PASSENGER_ON_BOARD="PASSENGER_ON_BOARD";
	public static final String AT_DROPOFF="AT_DROPOFF";
	public static final String COMPLETED="COMPLETED";
	public static final String CANCELED="CANCELED";
	public static final String AT_PICKUP="AT_PICKUP";	
}