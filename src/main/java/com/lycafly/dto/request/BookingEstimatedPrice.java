package com.lycafly.dto.request;

// TODO: Auto-generated Javadoc
/**
 * The Class BookingEstimatedPrice.
 */
public class BookingEstimatedPrice {

	private String lowerBound;

	public String getLowerBound() {
		return lowerBound;
	}

	public void setLowerBound(String lowerBound) {
		this.lowerBound = lowerBound;
	}

	public String getUpperBound() {
		return upperBound;
	}

	public void setUpperBound(String upperBound) {
		this.upperBound = upperBound;
	}

	private String upperBound;

}
