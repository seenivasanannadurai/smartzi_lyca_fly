package com.lycafly.dto.request;

// TODO: Auto-generated Javadoc

/**
 * The Class Constraints.
 */
public class Constraints {

	/** The passengers no. */
	private Integer passengers_no;
	
	/** The suitcases no. */
	private Integer suitcases_no;

	/**
	 * Gets the passengers no.
	 *
	 * @return the passengers no
	 */
	public Integer getPassengers_no() {
		return passengers_no;
	}

	/**
	 * Sets the passengers no.
	 *
	 * @param passengers_no the new passengers no
	 */
	public void setPassengers_no(Integer passengers_no) {
		this.passengers_no = passengers_no;
	}

	/**
	 * Gets the suitcases no.
	 *
	 * @return the suitcases no
	 */
	public Integer getSuitcases_no() {
		return suitcases_no;
	}

	/**
	 * Sets the suitcases no.
	 *
	 * @param suitcases_no the new suitcases no
	 */
	public void setSuitcases_no(Integer suitcases_no) {
		this.suitcases_no = suitcases_no;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Constraints [passengers_no=" + passengers_no + ", suitcases_no=" + suitcases_no + "]";
	}

	
	
	
}
