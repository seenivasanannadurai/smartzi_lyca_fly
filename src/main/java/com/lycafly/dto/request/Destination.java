package com.lycafly.dto.request;

// TODO: Auto-generated Javadoc

/**
 * The Class Destination.
 */
public class Destination {

	Flight flight;

	/** The point. */
	private Point point;

	/** The address. */
	private Address address;

	/** The place. */
	private DestinationPlace place;

	/** The free text. */
	private String free_text;

	/**
	 * Gets the point.
	 *
	 * @return the point
	 */
	public Point getPoint() {
		return point;
	}

	/**
	 * Sets the point.
	 *
	 * @param point
	 *            the new point
	 */
	public void setPoint(Point point) {
		this.point = point;
	}

	/**
	 * Gets the address.
	 *
	 * @return the address
	 */
	public Address getAddress() {
		return address;
	}

	/**
	 * Sets the address.
	 *
	 * @param address
	 *            the new address
	 */
	public void setAddress(Address address) {
		this.address = address;
	}

	/**
	 * Gets the place.
	 *
	 * @return the place
	 */
	public DestinationPlace getPlace() {
		return place;
	}

	/**
	 * Sets the place.
	 *
	 * @param place
	 *            the new place
	 */
	public void setPlace(DestinationPlace place) {
		this.place = place;
	}

	/**
	 * Gets the free text.
	 *
	 * @return the free text
	 */
	public String getFree_text() {
		return free_text;
	}

	/**
	 * Sets the free text.
	 *
	 * @param free_text
	 *            the new free text
	 */
	public void setFree_text(String free_text) {
		this.free_text = free_text;
	}

	public Flight getFlight() {
		return flight;
	}

	public void setFlight(Flight flight) {
		this.flight = flight;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Destination [point=" + point + ", address=" + address + ", place=" + place + ", free_text=" + free_text
				+ "]";
	}

}
