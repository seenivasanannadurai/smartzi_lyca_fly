package com.lycafly.dto.request;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBDocument;

import java.util.Arrays;

// TODO: Auto-generated Javadoc

/**
 * The Class DestinationAddress.
 */
@DynamoDBDocument
public class DestinationAddress {
	
	/** The country. */
	private String country;
	
	/** The country code. */
	private String country_code;
	
	/** The state. */
	private String state;
	
	/** The county. */
	private String county;
	
	/** The city. */
	private String city;
	
	/** The district. */
	private String district;
	
	/** The sub district. */
	private String sub_district;
	
	/** The street. */
	private String street;
	
	/** The house number. */
	private String house_number;
	
	/** The postal code. */
	private String postal_code;
	
	/** The building name. */
	private String building_name;
	
	/** The line. */
	private String[] line;
	
	/**
	 * Gets the country.
	 *
	 * @return the country
	 */
	public String getCountry() {
		return country;
	}

	/**
	 * Sets the country.
	 *
	 * @param country the new country
	 */
	public void setCountry(String country) {
		this.country = country;
	}

	/**
	 * Gets the country code.
	 *
	 * @return the country code
	 */
	public String getCountry_code() {
		return country_code;
	}

	/**
	 * Sets the country code.
	 *
	 * @param country_code the new country code
	 */
	public void setCountry_code(String country_code) {
		this.country_code = country_code;
	}

	/**
	 * Gets the state.
	 *
	 * @return the state
	 */
	public String getState() {
		return state;
	}

	/**
	 * Sets the state.
	 *
	 * @param state the new state
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * Gets the county.
	 *
	 * @return the county
	 */
	public String getCounty() {
		return county;
	}

	/**
	 * Sets the county.
	 *
	 * @param county the new county
	 */
	public void setCounty(String county) {
		this.county = county;
	}

	/**
	 * Gets the city.
	 *
	 * @return the city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * Sets the city.
	 *
	 * @param city the new city
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * Gets the district.
	 *
	 * @return the district
	 */
	public String getDistrict() {
		return district;
	}

	/**
	 * Sets the district.
	 *
	 * @param district the new district
	 */
	public void setDistrict(String district) {
		this.district = district;
	}

	/**
	 * Gets the sub district.
	 *
	 * @return the sub district
	 */
	public String getSub_district() {
		return sub_district;
	}

	/**
	 * Sets the sub district.
	 *
	 * @param sub_district the new sub district
	 */
	public void setSub_district(String sub_district) {
		this.sub_district = sub_district;
	}

	/**
	 * Gets the street.
	 *
	 * @return the street
	 */
	public String getStreet() {
		return street;
	}

	/**
	 * Sets the street.
	 *
	 * @param street the new street
	 */
	public void setStreet(String street) {
		this.street = street;
	}

	/**
	 * Gets the house number.
	 *
	 * @return the house number
	 */
	public String getHouse_number() {
		return house_number;
	}

	/**
	 * Sets the house number.
	 *
	 * @param house_number the new house number
	 */
	public void setHouse_number(String house_number) {
		this.house_number = house_number;
	}

	/**
	 * Gets the postal code.
	 *
	 * @return the postal code
	 */
	public String getPostal_code() {
		return postal_code;
	}

	/**
	 * Sets the postal code.
	 *
	 * @param postal_code the new postal code
	 */
	public void setPostal_code(String postal_code) {
		this.postal_code = postal_code;
	}

	/**
	 * Gets the building name.
	 *
	 * @return the building name
	 */
	public String getBuilding_name() {
		return building_name;
	}

	/**
	 * Sets the building name.
	 *
	 * @param building_name the new building name
	 */
	public void setBuilding_name(String building_name) {
		this.building_name = building_name;
	}

	/**
	 * Gets the line.
	 *
	 * @return the line
	 */
	public String[] getLine() {
		return line;
	}

	/**
	 * Sets the line.
	 *
	 * @param line the new line
	 */
	public void setLine(String[] line) {
		this.line = line;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Address [country=" + country + ", country_code=" + country_code + ", state=" + state + ", county="
				+ county + ", city=" + city + ", district=" + district + ", sub_district=" + sub_district + ", street="
				+ street + ", house_number=" + house_number + ", postal_code=" + postal_code + ", building_name="
				+ building_name + ", line=" + Arrays.toString(line) + "]";
	}

	
}
