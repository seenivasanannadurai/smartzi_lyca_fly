package com.lycafly.dto.request;

import org.springframework.stereotype.Service;

// TODO: Auto-generated Javadoc

/**
 * The Class DispatchRidesOffer.
 */
@Service
public class DispatchRidesOffer {

	/** The accepted and looking for driver. */
	private String accepted_and_looking_for_driver;
	
	/** The pickup eta. */
	private PickupEta pickup_eta;
	
	/** The booking estimated price. */
	private BookingEstimatedPrice booking_estimated_price;

	/**
	 * Gets the accepted and looking for driver.
	 *
	 * @return the accepted and looking for driver
	 */
	public String getAccepted_and_looking_for_driver() {
		return accepted_and_looking_for_driver;
	}

	/**
	 * Sets the accepted and looking for driver.
	 *
	 * @param accepted_and_looking_for_driver the new accepted and looking for driver
	 */
	public void setAccepted_and_looking_for_driver(String accepted_and_looking_for_driver) {
		this.accepted_and_looking_for_driver = accepted_and_looking_for_driver;
	}

	/**
	 * Gets the pickup eta.
	 *
	 * @return the pickup eta
	 */
	public PickupEta getPickup_eta() {
		return pickup_eta;
	}

	/**
	 * Sets the pickup eta.
	 *
	 * @param pickup_eta the new pickup eta
	 */
	public void setPickup_eta(PickupEta pickup_eta) {
		this.pickup_eta = pickup_eta;
	}

	/**
	 * Gets the booking estimated price.
	 *
	 * @return the booking estimated price
	 */
	public BookingEstimatedPrice getBooking_estimated_price() {
		return booking_estimated_price;
	}

	/**
	 * Sets the booking estimated price.
	 *
	 * @param booking_estimated_price the new booking estimated price
	 */
	public void setBooking_estimated_price(BookingEstimatedPrice booking_estimated_price) {
		this.booking_estimated_price = booking_estimated_price;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "DispatchRidesOffer [accepted_and_looking_for_driver=" + accepted_and_looking_for_driver
				+ ", pickup_eta=" + pickup_eta + ", booking_estimated_price=" + booking_estimated_price + "]";
	}
	
	
}
