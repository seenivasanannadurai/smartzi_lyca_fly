package com.lycafly.dto.request;

public class OfferRequest {

	private String request_id;

	private String supplier_id;

	Pickup pickup;

	Destination destination;

	Constraints constraints;

	PickupTime pickup_time;

	public String getrequest_id() {
		return request_id;
	}

	/**
	 * Sets the request id.
	 *
	 * @param request_id
	 *            the new request id
	 */
	public void setrequest_id(String request_id) {
		this.request_id = request_id;
	}

	/**
	 * Gets the supplier id.
	 *
	 * @return the supplier id
	 */
	public String getSupplier_id() {
		return supplier_id;
	}

	/**
	 * Sets the supplier id.
	 *
	 * @param supplier_id
	 *            the new supplier id
	 */
	public void setSupplier_id(String supplier_id) {
		this.supplier_id = supplier_id;
	}

	/**
	 * Gets the ride details.
	 *
	 * @return the ride details
	 */
	public Pickup getpickup() {
		return pickup;
	}

	/**
	 * Sets the pickup details.
	 *
	 * @param pickup
	 */
	public void setpickup(Pickup pickup) {
		this.pickup = pickup;
	}

	/**
	 * Gets the the destination details.
	 *
	 * @return destination
	 */
	public Destination getDestination() {
		return destination;
	}

	/**
	 * Sets the destination details
	 *
	 * @param destination
	 */
	public void setDestination(Destination destination) {
		this.destination = destination;
	}

	/**
	 * Gets the passenger details.
	 *
	 * @return destination
	 */
	public Constraints getConstraints() {
		return constraints;
	}

	/**
	 * Sets the destination details
	 *
	 * @param destination
	 */
	public void setConstraints(Constraints constraints) {
		this.constraints = constraints;
	}

	/**
	 * Gets the passenger details.
	 *
	 * @return pickupTime
	 */
	public PickupTime getpickupTime() {
		return pickup_time;
	}

	/**
	 * Sets the destination details
	 *
	 * @param destination
	 */
	public void setpickup_time(PickupTime pickup_time) {
		this.pickup_time = pickup_time;
	}

}
