package com.lycafly.dto.request;

// TODO: Auto-generated Javadoc
/**
 * The Class PassengerDetails.
 */
public class PassengerDetails {
	private String name;

	private String phoneNumber;
	
	private String email;
	
	private String referenceNo;
	
	public String getReferenceNo() {
		return referenceNo;
	}

	public void setReferenceNo(String referenceNo) {
		this.referenceNo = referenceNo;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	
}
