package com.lycafly.dto.request;

// TODO: Auto-generated Javadoc

/**
 * The Class Pickup.
 */
public class Pickup {

	Flight flight;

	public Flight getFlight() {
		return flight;
	}

	public void setFlight(Flight flight) {
		this.flight = flight;
	}

	/** The point. */
	private Point point;

	/** The address. */
	private Address address;

	/**
	 * Gets the point.
	 *
	 * @return the point
	 */
	public Point getPoint() {
		return point;
	}

	/**
	 * Sets the point.
	 *
	 * @param point
	 *            the new point
	 */
	public void setPoint(Point point) {
		this.point = point;
	}

	/**
	 * Gets the address.
	 *
	 * @return the address
	 */
	public Address getAddress() {
		return address;
	}

	/**
	 * Sets the address.
	 *
	 * @param address
	 *            the new address
	 */
	public void setAddress(Address address) {
		this.address = address;
	}

}
