package com.lycafly.dto.request;

// TODO: Auto-generated Javadoc
/**
 * The Class PickupEta.
 */
public class PickupEta {
	
	/** The seconds since epoch. */
	private String seconds_since_epoch;

	/**
	 * Gets the seconds since epoch.
	 *
	 * @return the seconds since epoch
	 */
	public String getSeconds_since_epoch() {
		return seconds_since_epoch;
	}

	/**
	 * Sets the seconds since epoch.
	 *
	 * @param seconds_since_epoch the new seconds since epoch
	 */
	public void setSeconds_since_epoch(String seconds_since_epoch) {
		this.seconds_since_epoch = seconds_since_epoch;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "PickupEta [seconds_since_epoch=" + seconds_since_epoch + "]";
	}
	
	
}
