package com.lycafly.dto.request;


// TODO: Auto-generated Javadoc
/**
 * The Class PickupTime.
 */
public class PickupTime {
	
	private String secondsSinceEpoch;

	public String getSecondsSinceEpoch() {
		return secondsSinceEpoch;
	}

	public void setSecondsSinceEpoch(String secondsSinceEpoch) {
		this.secondsSinceEpoch = secondsSinceEpoch;
	}

	

}
