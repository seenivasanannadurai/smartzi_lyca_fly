package com.lycafly.dto.request;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBDocument;

// TODO: Auto-generated Javadoc

/**
 * The Class Point.
 */
@DynamoDBDocument
public class Point {
	
	/** The lat. */
	private Double lat;
	
	/** The lng. */
	private Double lng;

	/**
	 * Gets the lat.
	 *
	 * @return the lat
	 */
	public Double getLat() {
		return lat;
	}

	/**
	 * Sets the lat.
	 *
	 * @param lat the new lat
	 */
	public void setLat(Double lat) {
		this.lat = lat;
	}

	/**
	 * Gets the lng.
	 *
	 * @return the lng
	 */
	public Double getLng() {
		return lng;
	}

	/**
	 * Sets the lng.
	 *
	 * @param lng the new lng
	 */
	public void setLng(Double lng) {
		this.lng = lng;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Point [lat=" + lat + ", lng=" + lng + "]";
	}

}
