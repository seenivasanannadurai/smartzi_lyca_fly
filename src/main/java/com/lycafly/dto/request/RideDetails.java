package com.lycafly.dto.request;

// TODO: Auto-generated Javadoc

/**
 * The Class RideDetails.
 */
public class RideDetails {

	/** The pickup. */
	private Pickup pickup;

	/** The destination. */
	private Destination destination;

	/** The constraints. */
	private Constraints constraints;

	/** The passenger details. */
	private PassengerDetails passenger_details;

	/** The pickup time. */
	private PickupTime pickup_time;

	/** The passenger note. */
	private String passenger_note;

	/** The dispatcher offer id. */
	private String dispatcher_offer_id;

	/** The payment flow. */
	private String payment_flow;

	/**
	 * Gets the pickup.
	 *
	 * @return the pickup
	 */
	public Pickup getPickup() {
		return pickup;
	}

	/**
	 * Sets the pickup.
	 *
	 * @param pickup the new pickup
	 */
	public void setPickup(Pickup pickup) {
		this.pickup = pickup;
	}

	/**
	 * Gets the destination.
	 *
	 * @return the destination
	 */
	public Destination getDestination() {
		return destination;
	}

	/**
	 * Sets the destination.
	 *
	 * @param destination the new destination
	 */
	public void setDestination(Destination destination) {
		this.destination = destination;
	}

	/**
	 * Gets the constraints.
	 *
	 * @return the constraints
	 */
	public Constraints getConstraints() {
		return constraints;
	}

	/**
	 * Sets the constraints.
	 *
	 * @param constraints the new constraints
	 */
	public void setConstraints(Constraints constraints) {
		this.constraints = constraints;
	}

	/**
	 * Gets the passenger details.
	 *
	 * @return the passenger details
	 */
	public PassengerDetails getPassenger_details() {
		return passenger_details;
	}

	/**
	 * Sets the passenger details.
	 *
	 * @param passenger_details the new passenger details
	 */
	public void setPassenger_details(PassengerDetails passenger_details) {
		this.passenger_details = passenger_details;
	}

	/**
	 * Gets the pickup time.
	 *
	 * @return the pickup time
	 */
	public PickupTime getPickup_time() {
		return pickup_time;
	}

	/**
	 * Sets the pickup time.
	 *
	 * @param pickup_time the new pickup time
	 */
	public void setPickup_time(PickupTime pickup_time) {
		this.pickup_time = pickup_time;
	}

	/**
	 * Gets the passenger note.
	 *
	 * @return the passenger note
	 */
	public String getPassenger_note() {
		return passenger_note;
	}

	/**
	 * Sets the passenger note.
	 *
	 * @param passenger_note the new passenger note
	 */
	public void setPassenger_note(String passenger_note) {
		this.passenger_note = passenger_note;
	}

	/**
	 * Gets the dispatcher offer id.
	 *
	 * @return the dispatcher offer id
	 */
	public String getDispatcher_offer_id() {
		return dispatcher_offer_id;
	}

	/**
	 * Sets the dispatcher offer id.
	 *
	 * @param dispatcher_offer_id the new dispatcher offer id
	 */
	public void setDispatcher_offer_id(String dispatcher_offer_id) {
		this.dispatcher_offer_id = dispatcher_offer_id;
	}

	/**
	 * Gets the payment flow.
	 *
	 * @return the payment flow
	 */
	public String getPayment_flow() {
		return payment_flow;
	}

	/**
	 * Sets the payment flow.
	 *
	 * @param payment_flow the new payment flow
	 */
	public void setPayment_flow(String payment_flow) {
		this.payment_flow = payment_flow;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "RideDetails [pickup=" + pickup + ", destination=" + destination + ", constraints=" + constraints
				+ ", passenger_details=" + passenger_details + ", pickup_time=" + pickup_time + ", passenger_note="
				+ passenger_note + ", dispatcher_offer_id=" + dispatcher_offer_id + ", payment_flow=" + payment_flow
				+ "]";
	}
	
	
	
	
}
