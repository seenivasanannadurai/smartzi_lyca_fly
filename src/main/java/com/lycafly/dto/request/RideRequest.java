package com.lycafly.dto.request;

import com.smartzi.dto.request.Passenger;

public class RideRequest {

	private String supplierId;

	private String flightBookReference;

	public String getFlightBookReference() {
		return flightBookReference;
	}

	public void setFlightBookReference(String flightBookReference) {
		this.flightBookReference = flightBookReference;
	}

	private Pickup pickup;

	private Destination destination;

	private Passenger passenger;

	private PickupTime pickupTime;

	private String vehicleType;

	private Operator operator;

	public Operator getOperator() {
		return operator;
	}

	public void setOperator(Operator operator) {
		this.operator = operator;
	}

	public String getSupplierId() {
		return supplierId;
	}

	public void setSupplierId(String supplierId) {
		this.supplierId = supplierId;
	}

	public Pickup getPickup() {
		return pickup;
	}

	public void setPickup(Pickup pickup) {
		this.pickup = pickup;
	}

	public Destination getDestination() {
		return destination;
	}

	public void setDestination(Destination destination) {
		this.destination = destination;
	}

	public Passenger getPassenger() {
		return passenger;
	}

	public void setPassenger(Passenger passenger) {
		this.passenger = passenger;
	}

	public String getPassengerNote() {
		return passengerNote;
	}

	public void setPassengerNote(String passengerNote) {
		this.passengerNote = passengerNote;
	}

	public String getVehicleType() {
		return vehicleType;
	}

	public void setVehicleType(String vehicleType) {
		this.vehicleType = vehicleType;
	}

	public PickupTime getPickupTime() {
		return pickupTime;
	}

	public void setPickupTime(PickupTime pickupTime) {
		this.pickupTime = pickupTime;
	}

	private String passengerNote;
}
