package com.lycafly.dto.request;

// TODO: Auto-generated Javadoc

/**
 * DTO to hold taxi booking request.
 */
public class TaxiBookingRequest {

    /** The first name. */
    private String firstName;

    /** The last name. */
    private String lastName;

    /** The country code. */
    private String countryCode;

    /** The mobile no. */
    private String mobileNo;

    /** The email id. */
    private String emailId;

    /** The trip enquiry request. */
    private TripEnquiryRequest tripEnquiryRequest;

    /**
     * Gets the first name.
     *
     * @return the firstName
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Sets the first name.
     *
     * @param firstName            the firstName to set
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * Gets the last name.
     *
     * @return the lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Sets the last name.
     *
     * @param lastName            the lastName to set
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * Gets the country code.
     *
     * @return the countryCode
     */
    public String getCountryCode() {
        return countryCode;
    }

    /**
     * Sets the country code.
     *
     * @param countryCode            the countryCode to set
     */
    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    /**
     * Gets the mobile no.
     *
     * @return the mobileNo
     */
    public String getMobileNo() {
        return mobileNo;
    }

    /**
     * Sets the mobile no.
     *
     * @param mobileNo            the mobileNo to set
     */
    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    /**
     * Gets the email id.
     *
     * @return the emailId
     */
    public String getEmailId() {
        return emailId;
    }

    /**
     * Sets the email id.
     *
     * @param emailId            the emailId to set
     */
    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    /**
     * Gets the trip enquiry request.
     *
     * @return the tripEnquiryRequest
     */
    public TripEnquiryRequest getTripEnquiryRequest() {
        return tripEnquiryRequest;
    }

    /**
     * Sets the trip enquiry request.
     *
     * @param tripEnquiryRequest            the tripEnquiryRequest to set
     */
    public void setTripEnquiryRequest(TripEnquiryRequest tripEnquiryRequest) {
        this.tripEnquiryRequest = tripEnquiryRequest;
    }

}
