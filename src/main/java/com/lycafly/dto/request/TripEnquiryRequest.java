package com.lycafly.dto.request;

// TODO: Auto-generated Javadoc
/**
 * DTO to hold trip request data.
 */
public class TripEnquiryRequest {
    
    /** The trip request id. */
    private Long tripRequestId;
    
    /** The current location lat. */
    private Double currentLocationLat;
    
    /** The current location long. */
    private Double currentLocationLong;
    
    /** The current location alt. */
    private Double currentLocationAlt;
    
    /** The source address. */
    private String sourceAddress;
    
    /** The source lat. */
    private Double sourceLat;
    
    /** The source long. */
    private Double sourceLong;
    
    /** The source time zone. */
    private String sourceTimeZone;
    
    /** The destination address. */
    private String destinationAddress;
    
    /** The destination lat. */
    private Double destinationLat;
    
    /** The destination long. */
    private Double destinationLong;
    
    /** The customer id. */
    private Long customerId;
    
    /** The trip req date time. */
    private String tripReqDateTime;
    
    /** The travel date time. */
    private String travelDateTime;
    
    /** The distance. */
    private Double distance;
    
    /** The estimated time. */
    private String estimatedTime;
    
    /** The utc travel date time. */
    private String utcTravelDateTime;
    
    /** The source post code. */
    private String sourcePostCode;
    
    /** The dest post code. */
    private String destPostCode;
    
    /** The airport trip. */
    private String airportTrip;
       
       /** The flight number. */
    private String flightNumber;
       
       /** The flight datetime. */
    private String flightDatetime;
       
       /** The passanger count. */
    private Integer passangerCount;
       
       /** The lauggage count. */
    private Integer lauggageCount;
    
    /** The vehicle type. */
	private String vehicleType;
	
	/** The booking note. */
   private String bookingNote;
	 
 	/** The booking amount. */
	 private double bookingAmount;
	 
 	/** The customer notification. */
	 private boolean customerNotification;
	 
 	/** The driver notification. */
	  private boolean driverNotification; 
	  
  	/** The controller booking trip. */
	private boolean controllerBookingTrip;
	
	/** The booking mode. */
	private String bookingMode;
	
	/** The stripe pay token. */
	private String stripePayToken;
	
	/** The payment type id. */
    private Short paymentTypeId;
    
    /** The recurring payment allowed. */
    private Boolean recurringPaymentAllowed;
    
    /** The referral code. */
    private String referralCode;
    
    /** The account name. */
    private String accountName;
    
    /** The passenger name. */
    private String passengerName;
    
    /** The passenger no. */
    private String passengerNo;
    
    /** The driver price. */
    private double driverPrice;
    
    /** The admin notes. */
    private String adminNotes;

    /**
     * Gets the vehicle type.
     *
     * @return the vehicle type
     */
    public String getVehicleType() {
        return vehicleType;
    }

    /**
     * Sets the vehicle type.
     *
     * @param vehicleType the new vehicle type
     */
    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }
          
    /**
     * Gets the airport trip.
     *
     * @return the airportTrip
     */
    public String getAirportTrip() {
        return airportTrip;
    }

    /**
     * Sets the airport trip.
     *
     * @param airportTrip            the airportTrip to set
     */
    public void setAirportTrip(String airportTrip) {
        this.airportTrip = airportTrip;
    }

    
    /**
     * Gets the flight number.
     *
     * @return the flightNumber
     */
    public String getFlightNumber() {
        return flightNumber;
    }

    /**
     * Sets the flight number.
     *
     * @param flightNumber            the flightNumber to set
     */
    public void setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
    }
    
    /**
     * Gets the passanger count.
     *
     * @return the passangerCount
     */
    public Integer getPassangerCount() {
        return passangerCount;
    }

    /**
     * Sets the passanger count.
     *
     * @param passangerCount            the passangerCount to set
     */
    public void setPassangerCount(Integer passangerCount) {
        this.passangerCount = passangerCount;
    }
    
    /**
     * Gets the lauggage count.
     *
     * @return the lauggageCount
     */
    public Integer getLauggageCount() {
        return lauggageCount;
    }

    /**
     * Sets the lauggage count.
     *
     * @param lauggageCount            the lauggageCount to set
     */
    public void setLauggageCount(Integer lauggageCount) {
        this.lauggageCount = lauggageCount;
    }
    
    
    /**
     * Gets the flight datetime.
     *
     * @return the flightDatetime
     */
    public String getFlightDatetime() {
        return flightDatetime;
    }

    /**
     * Sets the flight datetime.
     *
     * @param flightDatetime            the flightDatetime to set
     */
    public void setFlightDatetime(String flightDatetime) {
        this.flightDatetime = flightDatetime;
    }
    
    /**
     * Gets the trip request id.
     *
     * @return the tripRequestId
     */
    public Long getTripRequestId() {
        return tripRequestId;
    }

    /**
     * Sets the trip request id.
     *
     * @param tripRequestId            the tripRequestId to set
     */
    public void setTripRequestId(Long tripRequestId) {
        this.tripRequestId = tripRequestId;
    }

    /**
     * Gets the current location lat.
     *
     * @return the currentLocationLat
     */
    public Double getCurrentLocationLat() {
        return currentLocationLat;
    }

    /**
     * Sets the current location lat.
     *
     * @param currentLocationLat            the currentLocationLat to set
     */
    public void setCurrentLocationLat(Double currentLocationLat) {
        this.currentLocationLat = currentLocationLat;
    }

    /**
     * Gets the current location long.
     *
     * @return the currentLocationLong
     */
    public Double getCurrentLocationLong() {
        return currentLocationLong;
    }

    /**
     * Sets the current location long.
     *
     * @param currentLocationLong            the currentLocationLong to set
     */
    public void setCurrentLocationLong(Double currentLocationLong) {
        this.currentLocationLong = currentLocationLong;
    }

    /**
     * Gets the current location alt.
     *
     * @return the currentLocationAlt
     */
    public Double getCurrentLocationAlt() {
        return currentLocationAlt;
    }

    /**
     * Sets the current location alt.
     *
     * @param currentLocationAlt            the currentLocationAlt to set
     */
    public void setCurrentLocationAlt(Double currentLocationAlt) {
        this.currentLocationAlt = currentLocationAlt;
    }

    /**
     * Gets the source address.
     *
     * @return the sourceAddress
     */
    public String getSourceAddress() {
        return sourceAddress;
    }

    /**
     * Sets the source address.
     *
     * @param sourceAddress            the sourceAddress to set
     */
    public void setSourceAddress(String sourceAddress) {
        this.sourceAddress = sourceAddress;
    }

    /**
     * Gets the source lat.
     *
     * @return the sourceLat
     */
    public Double getSourceLat() {
        return sourceLat;
    }

    /**
     * Sets the source lat.
     *
     * @param sourceLat            the sourceLat to set
     */
    public void setSourceLat(Double sourceLat) {
        this.sourceLat = sourceLat;
    }

    /**
     * Gets the source long.
     *
     * @return the sourceLong
     */
    public Double getSourceLong() {
        return sourceLong;
    }

    /**
     * Sets the source long.
     *
     * @param sourceLong            the sourceLong to set
     */
    public void setSourceLong(Double sourceLong) {
        this.sourceLong = sourceLong;
    }

    /**
     * Gets the source time zone.
     *
     * @return the sourceTimeZone
     */
    public String getSourceTimeZone() {
        return sourceTimeZone;
    }

    /**
     * Sets the source time zone.
     *
     * @param sourceTimeZone            the sourceTimeZone to set
     */
    public void setSourceTimeZone(String sourceTimeZone) {
        this.sourceTimeZone = sourceTimeZone;
    }

    /**
     * Gets the destination address.
     *
     * @return the destinationAddress
     */
    public String getDestinationAddress() {
        return destinationAddress;
    }

    /**
     * Sets the destination address.
     *
     * @param destinationAddress            the destinationAddress to set
     */
    public void setDestinationAddress(String destinationAddress) {
        this.destinationAddress = destinationAddress;
    }

    /**
     * Gets the destination lat.
     *
     * @return the destinationLat
     */
    public Double getDestinationLat() {
        return destinationLat;
    }

    /**
     * Sets the destination lat.
     *
     * @param destinationLat            the destinationLat to set
     */
    public void setDestinationLat(Double destinationLat) {
        this.destinationLat = destinationLat;
    }

    /**
     * Gets the destination long.
     *
     * @return the destinationLong
     */
    public Double getDestinationLong() {
        return destinationLong;
    }

    /**
     * Sets the destination long.
     *
     * @param destinationLong            the destinationLong to set
     */
    public void setDestinationLong(Double destinationLong) {
        this.destinationLong = destinationLong;
    }

    /**
     * Gets the customer id.
     *
     * @return the customerId
     */
    public Long getCustomerId() {
        return customerId;
    }

    /**
     * Sets the customer id.
     *
     * @param customerId            the customerId to set
     */
    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    /**
     * Gets the trip req date time.
     *
     * @return the tripReqDateTime
     */
    public String getTripReqDateTime() {
        return tripReqDateTime;
    }

    /**
     * Sets the trip req date time.
     *
     * @param tripReqDateTime            the tripReqDateTime to set
     */
    public void setTripReqDateTime(String tripReqDateTime) {
        this.tripReqDateTime = tripReqDateTime;
    }

    /**
     * Gets the travel date time.
     *
     * @return the travelDateTime
     */
    public String getTravelDateTime() {
        return travelDateTime;
    }

    /**
     * Sets the travel date time.
     *
     * @param travelDateTime            the travelDateTime to set
     */
    public void setTravelDateTime(String travelDateTime) {
        this.travelDateTime = travelDateTime;
    }

    /**
     * Gets the distance.
     *
     * @return the distance
     */
    public Double getDistance() {
        return distance;
    }

    /**
     * Sets the distance.
     *
     * @param distance            the distance to set
     */
    public void setDistance(Double distance) {
        this.distance = distance;
    }

    /**
     * Gets the estimated time.
     *
     * @return the estimatedTime
     */
    public String getEstimatedTime() {
        return estimatedTime;
    }

    /**
     * Sets the estimated time.
     *
     * @param estimatedTime            the estimatedTime to set
     */
    public void setEstimatedTime(String estimatedTime) {
        this.estimatedTime = estimatedTime;
    }

    /**
     * Gets the utc travel date time.
     *
     * @return the utcTravelDateTime
     */
    public String getUtcTravelDateTime() {
        return utcTravelDateTime;
    }

    /**
     * Sets the utc travel date time.
     *
     * @param utcTravelDateTime            the utcTravelDateTime to set
     */
    public void setUtcTravelDateTime(String utcTravelDateTime) {
        this.utcTravelDateTime = utcTravelDateTime;
    }

    /**
     * Gets the source post code.
     *
     * @return the sourcePostCode
     */
    public String getSourcePostCode() {
        return sourcePostCode;
    }

    /**
     * Sets the source post code.
     *
     * @param sourcePostCode            the sourcePostCode to set
     */
    public void setSourcePostCode(String sourcePostCode) {
        this.sourcePostCode = sourcePostCode;
    }

    /**
     * Gets the dest post code.
     *
     * @return the destPostCode
     */
    public String getDestPostCode() {
        return destPostCode;
    }

    /**
     * Sets the dest post code.
     *
     * @param destPostCode            the destPostCode to set
     */
    public void setDestPostCode(String destPostCode) {
        this.destPostCode = destPostCode;
    }

	/**
	 * Gets the booking note.
	 *
	 * @return the booking note
	 */
	public String getBookingNote() {
		return bookingNote;
	}

	/**
	 * Sets the booking note.
	 *
	 * @param bookingNote the new booking note
	 */
	public void setBookingNote(String bookingNote) {
		this.bookingNote = bookingNote;
	}

	/**
	 * Gets the booking amount.
	 *
	 * @return the booking amount
	 */
	public double getBookingAmount() {
		return bookingAmount;
	}

	/**
	 * Sets the booking amount.
	 *
	 * @param bookingAmount the new booking amount
	 */
	public void setBookingAmount(double bookingAmount) {
		this.bookingAmount = bookingAmount;
	}

	/**
	 * Checks if is customer notification.
	 *
	 * @return true, if is customer notification
	 */
	public boolean isCustomerNotification() {
		return customerNotification;
	}

	/**
	 * Sets the customer notification.
	 *
	 * @param customerNotification the new customer notification
	 */
	public void setCustomerNotification(boolean customerNotification) {
		this.customerNotification = customerNotification;
	}

	/**
	 * Checks if is driver notification.
	 *
	 * @return true, if is driver notification
	 */
	public boolean isDriverNotification() {
		return driverNotification;
	}

	/**
	 * Sets the driver notification.
	 *
	 * @param driverNotification the new driver notification
	 */
	public void setDriverNotification(boolean driverNotification) {
		this.driverNotification = driverNotification;
	}

	/**
	 * Checks if is controller booking trip.
	 *
	 * @return true, if is controller booking trip
	 */
	public boolean isControllerBookingTrip() {
		return controllerBookingTrip;
	}

	/**
	 * Sets the controller booking trip.
	 *
	 * @param controllerBookingTrip the new controller booking trip
	 */
	public void setControllerBookingTrip(boolean controllerBookingTrip) {
		this.controllerBookingTrip = controllerBookingTrip;
	}

	/**
	 * Gets the booking mode.
	 *
	 * @return the booking mode
	 */
	public String getBookingMode() {
		return bookingMode;
	}

	/**
	 * Sets the booking mode.
	 *
	 * @param bookingMode the new booking mode
	 */
	public void setBookingMode(String bookingMode) {
		this.bookingMode = bookingMode;
	}

	/**
	 * Gets the stripe pay token.
	 *
	 * @return the stripe pay token
	 */
	public String getStripePayToken() {
		return stripePayToken;
	}

	/**
	 * Sets the stripe pay token.
	 *
	 * @param stripePayToken the new stripe pay token
	 */
	public void setStripePayToken(String stripePayToken) {
		this.stripePayToken = stripePayToken;
	}

	/**
	 * Gets the payment type id.
	 *
	 * @return the payment type id
	 */
	public Short getPaymentTypeId() {
		return paymentTypeId;
	}

	/**
	 * Sets the payment type id.
	 *
	 * @param paymentTypeId the new payment type id
	 */
	public void setPaymentTypeId(Short paymentTypeId) {
		this.paymentTypeId = paymentTypeId;
	}

	/**
	 * Gets the recurring payment allowed.
	 *
	 * @return the recurring payment allowed
	 */
	public Boolean getRecurringPaymentAllowed() {
		return recurringPaymentAllowed;
	}

	/**
	 * Sets the recurring payment allowed.
	 *
	 * @param recurringPaymentAllowed the new recurring payment allowed
	 */
	public void setRecurringPaymentAllowed(Boolean recurringPaymentAllowed) {
		this.recurringPaymentAllowed = recurringPaymentAllowed;
	}

	/**
	 * Gets the referral code.
	 *
	 * @return the referral code
	 */
	public String getReferralCode() {
		return referralCode;
	}

	/**
	 * Sets the referral code.
	 *
	 * @param referralCode the new referral code
	 */
	public void setReferralCode(String referralCode) {
		this.referralCode = referralCode;
	}

	/**
	 * Gets the account name.
	 *
	 * @return the account name
	 */
	public String getAccountName() {
		return accountName;
	}

	/**
	 * Sets the account name.
	 *
	 * @param accountName the new account name
	 */
	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	/**
	 * Gets the passenger name.
	 *
	 * @return the passenger name
	 */
	public String getPassengerName() {
		return passengerName;
	}

	/**
	 * Sets the passenger name.
	 *
	 * @param passengerName the new passenger name
	 */
	public void setPassengerName(String passengerName) {
		this.passengerName = passengerName;
	}

	/**
	 * Gets the passenger no.
	 *
	 * @return the passenger no
	 */
	public String getPassengerNo() {
		return passengerNo;
	}

	/**
	 * Sets the passenger no.
	 *
	 * @param passengerNo the new passenger no
	 */
	public void setPassengerNo(String passengerNo) {
		this.passengerNo = passengerNo;
	}

	/**
	 * Gets the driver price.
	 *
	 * @return the driver price
	 */
	public double getDriverPrice() {
		return driverPrice;
	}

	/**
	 * Sets the driver price.
	 *
	 * @param driverPrice the new driver price
	 */
	public void setDriverPrice(double driverPrice) {
		this.driverPrice = driverPrice;
	}

	/**
	 * Gets the admin notes.
	 *
	 * @return the adminNotes
	 */
	public String getAdminNotes() {
		return adminNotes;
	}

	/**
	 * Sets the admin notes.
	 *
	 * @param adminNotes the adminNotes to set
	 */
	public void setAdminNotes(String adminNotes) {
		this.adminNotes = adminNotes;
	}
}
