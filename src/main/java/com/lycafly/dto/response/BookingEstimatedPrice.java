package com.lycafly.dto.response;

public class BookingEstimatedPrice {

	private int lower_bound;

	private int upper_bound;

	/**
	 * @param lower_bound
	 *            the lower_bound to set
	 */
	public void setlower_bound(int lower_bound) {
		this.lower_bound = lower_bound;
	}

	/**
	 * @return the lower_bound
	 */
	public int getlower_bound() {
		return lower_bound;
	}

	public void setupper_bound(int upper_bound) {
		this.upper_bound = upper_bound;
	}

	public int getupper_bound() {
		return upper_bound;
	}

}