package com.lycafly.dto.response;

public class CancelRideResponse {

	private String cancel_accepted;

	private String cancel_reason;

	public void setcancel_reason(String cancel_reason)

	{
		this.cancel_reason = cancel_reason;
	}

	public String getcancel_reason() {
		return cancel_reason;
	}

	public void setcancel_accepted(String cancel_accepted)

	{
		this.cancel_accepted = cancel_accepted;
	}

	public String getcancel_accepted() {
		return cancel_accepted;
	}

}
