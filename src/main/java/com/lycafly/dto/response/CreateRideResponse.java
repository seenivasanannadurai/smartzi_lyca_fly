package com.lycafly.dto.response;

import java.util.List;

import com.lycafly.dto.request.RecommendedDrivers;

public class CreateRideResponse {

	private Long requestId;

	private List<RecommendedDrivers> recommendedDrivers;

	public  List<RecommendedDrivers> getRecommendedDrivers() {
		return recommendedDrivers;
	}

	public void setRecommendedDrivers( List<RecommendedDrivers> recommendedDrivers) {
		this.recommendedDrivers = recommendedDrivers;
	}

	public Long getRequestId() {
		return requestId;
	}

	public void setRequestId(Long requestId) {
		this.requestId = requestId;
	}

	public BookingEstimatedPrice getBookingEstimatedPrice() {
		return bookingEstimatedPrice;
	}

	public void setBookingEstimatedPrice(BookingEstimatedPrice bookingEstimatedPrice) {
		this.bookingEstimatedPrice = bookingEstimatedPrice;
	}

	public EstimatedRideDurationSeconds getEstimatedRideDurationSeconds() {
		return estimatedRideDurationSeconds;
	}

	public void setEstimatedRideDurationSeconds(EstimatedRideDurationSeconds estimatedRideDurationSeconds) {
		this.estimatedRideDurationSeconds = estimatedRideDurationSeconds;
	}

	private BookingEstimatedPrice bookingEstimatedPrice;

	private EstimatedRideDurationSeconds estimatedRideDurationSeconds;

}
