package com.lycafly.dto.response;

public class DirectionAPIResponse {
	
	public double getDistance() {
		return distance;
	}

	public void setDistance(double distance) {
		this.distance = distance;
	}

	public String getEta() {
		return eta;
	}

	public void setEta(String eta) {
		this.eta = eta;
	}

	private double distance;
	
	private String eta;

}
