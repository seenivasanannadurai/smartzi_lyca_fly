package com.lycafly.dto.response;

public class EstimatedRideDurationSeconds{

	private int value;

	public void setValue(int value){
		this.value = value;
	}

	public int getValue(){
		return value;
	}

	
}