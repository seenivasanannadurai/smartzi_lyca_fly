package com.lycafly.dto.response;

// TODO: Auto-generated Javadoc
/**
 * The Class OAuthResponse.
 */
public class OAuthResponse {
    
    /** The access token. */
    private String access_token;
    
    /** The token type. */
    private String token_type;
    
    /** The refresh token. */
    private String refresh_token;
    
    /** The expires in. */
    private Integer expires_in;
    
    /** The scope. */
    private String scope;

    /**
     * Gets the access token.
     *
     * @return the access_token
     */
    public String getAccess_token() {
        return access_token;
    }

    /**
     * Sets the access token.
     *
     * @param access_token            the access_token to set
     */
    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }

    /**
     * Gets the token type.
     *
     * @return the token_type
     */
    public String getToken_type() {
        return token_type;
    }

    /**
     * Sets the token type.
     *
     * @param token_type            the token_type to set
     */
    public void setTokenType(String token_type) {
        this.token_type = token_type;
    }

    /**
     * Gets the refresh token.
     *
     * @return the refresh_token
     */
    public String getRefresh_token() {
        return refresh_token;
    }

    /**
     * Sets the refresh token.
     *
     * @param refresh_token            the refresh_token to set
     */
    public void setRefresh_token(String refresh_token) {
        this.refresh_token = refresh_token;
    }

    /**
     * Gets the expires in.
     *
     * @return the expires_in
     */
    public Integer getExpires_in() {
        return expires_in;
    }

    /**
     * Sets the expires in.
     *
     * @param expires_in            the expires_in to set
     */
    public void setExpires_in(Integer expires_in) {
        this.expires_in = expires_in;
    }

    /**
     * Gets the scope.
     *
     * @return the scope
     */
    public String getScope() {
        return scope;
    }

    /**
     * Sets the scope.
     *
     * @param scope            the scope to set
     */
    public void setScope(String scope) {
        this.scope = scope;
    }
}
