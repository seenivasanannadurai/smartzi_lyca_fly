package com.lycafly.dto.response;

import java.util.List;

public class OfferResponse {

	private List<Offers> offers;

	public void setOffers(List<Offers> offers) {
		this.offers = offers;
	}

	public List<Offers> getOffers() {
		return offers;
	}

}
