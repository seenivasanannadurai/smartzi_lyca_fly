package com.lycafly.dto.response;

public class Offers {
	private BookingEstimatedPrice booking_estimated_price;

	private String dispatcher_offer_id;

	private EstimatedRideDurationSeconds estimated_ride_duration_seconds;

	private PickupEtaSeconds pickup_eta_seconds;

	public void setbooking_estimated_price(BookingEstimatedPrice booking_estimated_price) {
		this.booking_estimated_price = booking_estimated_price;
	}

	public BookingEstimatedPrice getbooking_estimated_price() {
		return booking_estimated_price;
	}

	public void setdispatcher_offer_id(String dispatcher_offer_id) {
		this.dispatcher_offer_id = dispatcher_offer_id;
	}

	public String getdispatcher_offer_id() {
		return dispatcher_offer_id;
	}

	public void setestimated_ride_duration_seconds(EstimatedRideDurationSeconds estimated_ride_duration_seconds) {
		this.estimated_ride_duration_seconds = estimated_ride_duration_seconds;
	}

	public EstimatedRideDurationSeconds getestimated_ride_duration_seconds() {
		return estimated_ride_duration_seconds;
	}

	public void setpickup_eta_seconds(PickupEtaSeconds pickup_eta_seconds) {
		this.pickup_eta_seconds = pickup_eta_seconds;
	}

	public PickupEtaSeconds getpickup_eta_seconds() {
		return pickup_eta_seconds;
	}

}
