package com.lycafly.dto.response;

public class PickupEtaSeconds{

	private int value;

	public void setValue(int value){
		this.value = value;
	}

	public int getValue(){
		return value;
	}
}