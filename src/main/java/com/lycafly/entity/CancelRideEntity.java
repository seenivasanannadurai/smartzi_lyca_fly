package com.lycafly.entity;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAutoGeneratedKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;

import java.io.Serializable;

@DynamoDBTable(tableName = "b2b001-CancelRide")
public class CancelRideEntity implements Serializable {

	private static final long serialVersionUID = 1L;
	private String cancelRideId;

	private String ride_id;

	private String request;

	private String response;

	private String cancelReason;

	@DynamoDBHashKey(attributeName = "cancelRideId")
	@DynamoDBAutoGeneratedKey
	public String getCancelRideId() {
		return cancelRideId;
	}

	public void setCancelRideId(String cancelRideId) {
		this.cancelRideId = cancelRideId;
	}

	public String getRide_id() {
		return ride_id;
	}

	public void setRide_id(String ride_id) {
		this.ride_id = ride_id;
	}

	public String getRequest() {
		return request;
	}

	public void setRequest(String request) {
		this.request = request;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public String getCancelReason() {
		return cancelReason;
	}

	public void setCancelReason(String cancelReason) {
		this.cancelReason = cancelReason;
	}

}
