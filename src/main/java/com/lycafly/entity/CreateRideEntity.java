package com.lycafly.entity;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAutoGeneratedKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;

import java.io.Serializable;

@DynamoDBTable(tableName = "b2b001-CreateRide")
public class CreateRideEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	private String dispatcher_offer_id;
	private String rideRequest;
	private String rideResponse;
	private String request_id;
	private String supplier_id;
	private String exception;
	private String create_ride_id;
	private String ride_id;

	@DynamoDBHashKey(attributeName = "create_ride_id")
	@DynamoDBAutoGeneratedKey
	public String getcreate_ride_id() {
		return create_ride_id;
	}

	public void setcreate_ride_id(String create_ride_id) {
		this.create_ride_id = create_ride_id;
	}

	public String getdispatcher_offer_id() {
		return dispatcher_offer_id;
	}

	public void setdispatcher_offer_id(String dispatcher_offer_id) {
		this.dispatcher_offer_id = dispatcher_offer_id;
	}

	public String getRideRequest() {
		return rideRequest;
	}

	public void setRideRequest(String rideRequest) {
		this.rideRequest = rideRequest;
	}

	public String getrideResponse() {
		return rideResponse;
	}

	public void setrideResponse(String rideResponse) {
		this.rideResponse = rideResponse;
	}

	public String getrequest_id() {
		return request_id;
	}

	public void setrequest_id(String request_id) {
		this.request_id = request_id;
	}

	public String getsupplier_id() {
		return supplier_id;
	}

	public void setsupplier_id(String supplier_id) {
		this.supplier_id = supplier_id;
	}

	public String getexception() {
		return exception;
	}

	public void setexception(String exception) {
		this.exception = exception;
	}
	public String getride_id() {
		return ride_id;
	}

	public void setride_id(String ride_id) {
		this.ride_id = ride_id;
	}

}
