 package com.lycafly.entity;

 import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
 import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
 import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBRangeKey;
 import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;

 import java.io.Serializable;

 @DynamoDBTable(tableName = "b2b001-RideDetails")
 public class RideDetailsEntity implements Serializable
 {

     private static final long serialVersionUID = 1L;

     private Long request_id;
     private Long reservationId;
     private String exception;
     private String ride_id;
     private String supplier_id;

     @DynamoDBAttribute
     public Long getrequest_id() {
         return request_id;
     }

     public void setrequest_id(Long request_id) {
         this.request_id = request_id;
     }

     @DynamoDBAttribute
     public Long getReservationId() {
         return reservationId;
     }

     public void setReservationId(Long reservationId) {
         this.reservationId = reservationId;
     }
     @DynamoDBAttribute
     public String getexception() {
         return exception;
     }

     public void setexception(String exception) {
         this.exception = exception;
     }

     @DynamoDBHashKey(attributeName = "ride_id")
     public String getride_id() {
         return ride_id;
     }

     public void setride_id(String ride_id) {
         this.ride_id = ride_id;
     }

     @DynamoDBRangeKey
     public String getsupplier_id() {
         return supplier_id;
     }

     public void setsupplier_id(String supplier_id) {
         this.supplier_id = supplier_id;
     }

 }
