package com.lycafly.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBQueryExpression;
import com.lycafly.entity.CancelRideEntity;
import com.lycafly.entity.CreateRideEntity;
import com.lycafly.entity.RideDetailsEntity;
import com.lycafly.entity.RideStatusEntity;


@Repository
public class TripsRepository {
	@Autowired
	private DynamoDBMapper mapper;


	public String insertIntoRideRequest(CreateRideEntity createRideEntity) {
		mapper.save(createRideEntity);
		return createRideEntity.getcreate_ride_id();
	}

	public String insertIntoRideDetails(RideDetailsEntity rideDetails) {
		mapper.save(rideDetails);
		return rideDetails.getride_id();
	}

	public String inserRideStatusRequest(RideStatusEntity rideStatusEntity) {
		mapper.save(rideStatusEntity);
		return rideStatusEntity.getRideStatusId();

	}

	public String insertCancelRideRequest(CancelRideEntity cancelRideEntity) {
		mapper.save(cancelRideEntity);
		return cancelRideEntity.getCancelRideId();

	}

	public RideDetailsEntity getRideDetails(String ride_id) {
		RideDetailsEntity partitionKey = new RideDetailsEntity();
		partitionKey.setride_id(ride_id);
		DynamoDBQueryExpression<RideDetailsEntity> queryExpression = new DynamoDBQueryExpression<RideDetailsEntity>()
				.withHashKeyValues(partitionKey);
		return mapper.query(RideDetailsEntity.class, queryExpression).get(0);
	}

}
