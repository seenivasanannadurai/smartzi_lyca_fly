package com.lycafly.service;

import com.exception.SmartziException;
import com.lycafly.dto.request.BookRideRequest;
import com.lycafly.dto.request.CancelRideRequest;
import com.lycafly.dto.request.RideRequest;
import com.lycafly.dto.response.SmartziResponse;
import com.smartzi.dto.request.UpdateRideRequest;


public interface TripService {

	/**
	 * To request for new ride.
	 *
	 * @param hererideRequest
	 *            the rideRequest request
	 * @return SmartziResponse SmartziResponse
	 * @throws SmartziException
	 *             SmartziException
	 */
	SmartziResponse createRide(RideRequest rideRequest) throws SmartziException;

	/**
	 * Cancel the ride request
	 *
	 * @param cancelRideRequest
	 * @return SmartziResponse SmartziResponse
	 * @throws SmartziException
	 *             SmartziException
	 */
	SmartziResponse cancelRide(CancelRideRequest cancelRideRequest) throws SmartziException;

	/**
	 * get the ride status
	 * @throws SmartziException
	 *             SmartziException
	 */
	SmartziResponse getRideStatus(Long rideId) throws SmartziException;
	
	

	/**
	 get vehicle types supported by smartzi
	 */
	SmartziResponse getVehicleTypes() throws SmartziException;
	
	
	/**
	 * Cancel the ride request
	 *
	 * @param updateRideRequest
	 * @return SmartziResponse SmartziResponse
	 * @throws SmartziException
	 *             SmartziException
	 */
	SmartziResponse updateRide(UpdateRideRequest updateRideRequest) throws SmartziException;

	/**
	 * To request for new ride.
	 *
	 * @param bookRideRequest
	 *            the bookRideRequest request
	 * @return SmartziResponse SmartziResponse
	 * @throws SmartziException
	 *             SmartziException
	 */
	SmartziResponse bookRide(BookRideRequest bookRideRequest) throws SmartziException;



}
