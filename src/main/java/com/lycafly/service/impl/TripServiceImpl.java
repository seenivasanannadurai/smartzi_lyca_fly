package com.lycafly.service.impl;

import com.exception.SmartziException;
import com.lycafly.business.AdminManagement;
import com.lycafly.business.TripManagement;
import com.lycafly.constants.ApplicationConstant;
import com.lycafly.dto.request.BookRideRequest;
import com.lycafly.dto.request.CancelRideRequest;
import com.lycafly.dto.request.RideRequest;
import com.lycafly.dto.response.SmartziResponse;
import com.lycafly.service.TripService;
import com.lycafly.utils.SmartziUtil;
import com.smartzi.dto.request.UpdateRideRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TripServiceImpl implements TripService {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(TripServiceImpl.class);

	@Autowired
	private TripManagement tripManagement;

	@Autowired
	private AdminManagement adminManagement;

	@Override
	@PostMapping("/BookRide")
	public SmartziResponse bookRide(@RequestBody BookRideRequest bookRideRequest) throws SmartziException {
		LOGGER.info("Request Received:" + bookRideRequest);
		return SmartziUtil.generateResponse(adminManagement.bookRide(bookRideRequest), ApplicationConstant.SUCCESS,
				ApplicationConstant.SUCCESS_STATUS);
	}
	@Override
	@PostMapping("/RequestRide")
	public SmartziResponse createRide(@RequestBody RideRequest rideRequest) throws SmartziException {
		LOGGER.info("Request Received:" + rideRequest);
		return SmartziUtil.generateResponse(tripManagement.CreateRide(rideRequest), ApplicationConstant.SUCCESS,
				ApplicationConstant.SUCCESS_STATUS);
	}

	@Override
	@PostMapping("/CancelRide")
	public SmartziResponse cancelRide(@RequestBody CancelRideRequest cancelRequest) throws SmartziException {
		LOGGER.info("Request Received:" + cancelRequest);
		tripManagement.cancelRide(cancelRequest);
		return SmartziUtil.generateResponse(null, ApplicationConstant.SUCCESS,
				ApplicationConstant.SUCCESS_STATUS);
	}

	@Override
	@GetMapping("/GetRideStatus/{rideId}")
	public SmartziResponse getRideStatus(@PathVariable("rideId") Long rideId) throws SmartziException {
		// TODO Auto-generated method stub
		return SmartziUtil.generateResponse(tripManagement.getRideStatus(rideId), ApplicationConstant.SUCCESS,
				ApplicationConstant.SUCCESS_STATUS);
	}

	@Override
	@GetMapping("/GetVehicleTypes")
	public SmartziResponse getVehicleTypes() throws SmartziException {
		// TODO Auto-generated method stub
		return SmartziUtil.generateResponse(adminManagement.getVehicleTypes(), ApplicationConstant.SUCCESS,
				ApplicationConstant.SUCCESS_STATUS);
	}

	@Override
	@PostMapping("/UpdateRide")
	public SmartziResponse updateRide(@RequestBody UpdateRideRequest updateRideRequest) throws SmartziException {
		LOGGER.info("Request Received:" + updateRideRequest);
		tripManagement.updateRide(updateRideRequest);
		return SmartziUtil.generateResponse(null, ApplicationConstant.SUCCESS, ApplicationConstant.SUCCESS_STATUS);
	}

}
