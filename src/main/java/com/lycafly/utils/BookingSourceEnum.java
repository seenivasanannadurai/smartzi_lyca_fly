package com.lycafly.utils;


public enum BookingSourceEnum {

    APP(1001, "APP"), CP(1002, "CONTROL PANEL"), OUTSOURCE(1003, "OUT SOURCE");
    /**
     *
     */
    private final Integer id;
    /**
     *
     */
    private final String bookingSource;

    /**
     *
     */
    private BookingSourceEnum(Integer id, String bookingSource) {
        this.id = id;
        this.bookingSource = bookingSource;
    }

   
    public int getId() {
        return id;
    }

   
    public String getBookingSource() {
        return bookingSource;
    }
}
