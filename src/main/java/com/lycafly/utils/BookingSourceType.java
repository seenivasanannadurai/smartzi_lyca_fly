package com.lycafly.utils;


public enum BookingSourceType {

    CUSTOMER(1101, "CUSTOMER"), DRIVER(1102, "DRIVER"), OUTSOURCE(1105, "OUTSOURCE");
    /**
     *
     */
    private final Integer id;
    /**
     *
     */
    private final String bookingSourceType;

    /**
     *
     */
    private BookingSourceType(Integer id, String bookingSourceType) {
        this.id = id;
        this.bookingSourceType = bookingSourceType;
    }

    /**
     *
     */
    public int getId() {
        return id;
    }

    /**
     *
     */
    public String getBookingSourceType() {
        return bookingSourceType;
    }
}


