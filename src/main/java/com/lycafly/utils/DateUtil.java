package com.lycafly.utils;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.LocalDateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.time.Instant;

/**
 * Copyright (C) MiA - Chalico Ltd.
 * <p>
 * Confidential and proprietary.
 */

/**
 * Date utility to perform common date operations
 */

public class DateUtil {

	/**
	
	 *
	
	 */

	public static final DateTimeFormatter dateTimeformatter = DateTimeFormat.forPattern("dd-MM-yyyy HH:mm");
	/**
	
	 *
	
	 */

	public static final DateTimeFormatter dateFormatter = DateTimeFormat.forPattern("dd-MM-yyyy");
	/**
	 *
	 */
	public static final DateTimeFormatter timeFormatter = DateTimeFormat.forPattern("HH:mm");
	/**
	 *
	 */
	public static final DateTimeFormatter dayFormatter = DateTimeFormat.forPattern("EEEE");
	public static final DateTimeFormatter dateformat = DateTimeFormat.forPattern("MMM dd, yyyy");
	public static final DateTimeFormatter dateandtimeformat = DateTimeFormat.forPattern("MMM dd, yyyy HH:mm");
	public static final String SERVER_DATE_FORMAT = "dd-MM-yyyy";
	public static final DateTimeFormatter dateFormatterMonth = DateTimeFormat.forPattern("dd-MMM-yyyy");

	/**
	 *
	 */
	private DateUtil() {
		// private constructor
	}

	/**
	 * Formats passed utcDateTime to joda DateTime
	 *
	 * @param utcDateTime
	 *            Date time string in dd-MM-yyyy HH:mm format
	 * @return
	 */

	public static DateTime formatToJodaDateAndTime(String utcDateTime) {

		return dateTimeformatter.withZoneUTC().parseDateTime(utcDateTime);

	}

	/**
	 * Formats and returns UTC current date and time in dd-MM-yyyy format
	 *
	 * @return
	 */

	public static DateTime getUTCCurrentDateAndTime() {

		DateTime dt = new DateTime(DateTimeZone.UTC);

		return dt;

	}

	/**
	 * Formats and returns UTC current date and time in dd-MM-yyyy HH:mm format
	 *
	 * @return
	 */

	public static String getUTCCurrentDateAndTimeString() {

		DateTime dt = new DateTime(DateTimeZone.UTC);

		return dateTimeformatter.print(dt);

	}

	/**
	 * To get day for the passed UTC date time. Example: Sunday, Monday etc.
	 *
	 * @param utcDateStr
	 *            In dd-MM-yyyy format
	 * @return
	 */

	public static String getDayForDate(String utcDateStr) {

		return dayFormatter.withZoneUTC().print(dateFormatter.parseDateTime(utcDateStr));

	}

	/**
	 * To get day for the passed UTC date time. Example: Sunday, Monday etc.
	 *
	 * @param utcDateTime
	 *            In dd-MM-yyyy HH:mm format
	 * @return
	 */

	public static String getDayForDateTime(String utcDateTime) {

		return dayFormatter.withZoneUTC().print(dateTimeformatter.parseDateTime(utcDateTime));

	}

	/**
	 * To get day for the passed UTC date time. Example: Sunday, Monday etc.
	 *
	 * @param utcDateTime
	 *            In dd-MM-yyyy HH:mm format
	 * @return
	 */

	public static String getDayForDateTime(String utcDateTime, String timezone) {

		return dayFormatter.print(dateTimeformatter.withZoneUTC().parseDateTime(utcDateTime)

		.toDateTime(DateTimeZone.forID(timezone)));

	}

	/**
	 * To get day for the passed UTC date time. Example: Sunday, Monday etc.
	 *
	 * @param utcDateTime
	 *            In dd-MM-yyyy HH:mm format
	 * @return
	 */

	public static String getDateTimeForTimezone(String utcDateTime, String timezone) {

		return dateTimeformatter.print(dateTimeformatter.withZoneUTC().parseDateTime(utcDateTime)

		.toDateTime(DateTimeZone.forID(timezone)));

	}

	/**
	 * To get UTC Date and time for the passed dateTime which lies in the passed
	 * <p>
	 * timezone
	 *
	 * @param dateTime
	 *            In dd-MM-yyyy HH:mm format
	 * @param timezone
	 *            Timezone in which passed dateTime belongs to. Example:
	 *            <p>
	 *            Asia/Calcutta
	 * @return
	 */

	public static DateTime getUTCDateTimeForTimezone(String dateTime, String timezone) {

		DateTime dt = new LocalDateTime(dateTimeformatter.parseDateTime(dateTime))
				.toDateTime(DateTimeZone.forID(timezone));
		return dt.withZone(DateTimeZone.UTC);

	}

	/**
	 * To get String UTC Date and time for the passed dateTime which lies in the
	 * <p>
	 * passed timezone
	 *
	 * @param dateTime
	 *            In dd-MM-yyyy HH:mm format
	 * @param timezone
	 *            Timezone in which passed dateTime belongs to. Example:
	 *            <p>
	 *            Asia/Calcutta
	 * @return
	 */

	public static String getUTCDateTimeForTimezoneInString(String dateTime, String timezone) {

		return DateUtil.dateTimeformatter.print(getUTCDateTimeForTimezone(dateTime, timezone));

	}

	public static String convertTimeStampToDate(String timestamp) {
		DateTime dateTime = new DateTime(Long.parseLong(timestamp));
		return dateTimeformatter.print(dateTime);

	}

	public static Instant getTimeStamp() {
		Instant timestamp = Instant.now();
		System.out.println("Current Timestamp = " + timestamp);
		return timestamp;
	}

}