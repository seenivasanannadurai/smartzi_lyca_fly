package com.lycafly.utils;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.lycafly.dto.response.DirectionAPIResponse;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class MapUtil {

	/**
	*
	*/
	private static final String LOCATION_DIRECTION_API_URL = "https://maps.googleapis.com/maps/api/directions/json?";

	/**
	*
	*/
	private MapUtil() {
		// private constructor
	}

	public static double distance(double lat1, double lon1, double lat2, double lon2, String unit) {
		double theta = lon1 - lon2;
		double dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2))
				+ Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(theta));
		dist = Math.acos(dist);
		dist = rad2deg(dist);
		dist = dist * 60 * 1.1515;
		if (unit == "K") {
			dist = dist * 1.609344;
		} else if (unit == "N") {
			dist = dist * 0.8684;
		}

		return (round(dist, 1));
	}

	public static double round(double value, int places) {
		if (places < 0)
			throw new IllegalArgumentException();

		long factor = (long) Math.pow(10, places);
		value = value * factor;
		long tmp = Math.round(value);
		return (double) tmp / factor;
	}

	/* ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: */
	/* :: This function converts decimal degrees to radians : */
	/* ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: */
	private static double deg2rad(double deg) {
		return (deg * Math.PI / 180.0);
	}

	/* ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: */
	/* :: This function converts radians to decimal degrees : */
	/* ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: */
	private static double rad2deg(double rad) {
		return (rad * 180 / Math.PI);
	}

	/**
	 * Gives the distance between source and destination lat longs in miles
	 * 
	 * @param sourceLatitude
	 *            sourceLatitude
	 * @param sourceLongitude
	 *            sourceLongitude
	 * @param destLatitude
	 *            destLatitude
	 * @param destLongitude
	 *            destLongitude
	 * @return double distance
	 */
	public static double calculateDistance(double sourceLatitude, double sourceLongitude, double destLatitude,
			double destLongitude) {
		double earthRadiusInMile = 3959.0;
		double srcLatInRadian = Math.toRadians(sourceLatitude);
		double srcLngInRadian = Math.toRadians(sourceLongitude);
		double drvLatInRadian = Math.toRadians(destLatitude);
		double drvLngInRadian = Math.toRadians(destLongitude);
		double dlon = drvLngInRadian - srcLngInRadian;
		double dlat = drvLatInRadian - srcLatInRadian;
		double a = Math.sin(dlat / 2) * Math.sin(dlat / 2)
				+ Math.cos(srcLatInRadian) * Math.cos(drvLatInRadian) * Math.sin(dlon / 2) * Math.sin(dlon / 2);
		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
		return earthRadiusInMile * c;
	}

	public static String getETA(double drvLat, double drvLng, double destLat, double destLng, String directionAPIKey)
			throws org.json.simple.parser.ParseException {

		String parameters = "origin=" + drvLat + "," + drvLng + "&" + "destination=" + destLat + "," + destLng
				+ "&units=imperial" + "&key=" + directionAPIKey;

		String url = LOCATION_DIRECTION_API_URL + parameters;

		String duration = "0 min";
		JSONParser jsonParser = null;
		JSONObject jsonObject = null;
		try {
			jsonParser = new JSONParser();
			jsonObject = (JSONObject) jsonParser.parse(downloadUrl(url));
			JSONArray routes = (JSONArray) jsonObject.get("routes");
			JSONObject route = null;
			JSONArray legs = null;
			JSONObject leg = null;
			if (!routes.isEmpty()) {
				route = (JSONObject) routes.get(0);
				legs = (JSONArray) route.get("legs");
				leg = (JSONObject) legs.get(0);
				duration = (String) ((JSONObject) leg.get("duration")).get("text");
			}
		} catch (Exception e) {
		}
		return duration;
	}

	public static DirectionAPIResponse getDistance(double sourceLat, double sourceLong, double driverLat, double driverLong,
			String directionAPIKey) {
		double distance = 0;

		String parameters = "origin=" + sourceLat + "," + sourceLong + "&" + "destination=" + driverLat + ","
				+ driverLong + "&units=imperial" + "&key=" + directionAPIKey;

		String url = LOCATION_DIRECTION_API_URL + parameters;
		DirectionAPIResponse directionAPIResponse=new DirectionAPIResponse();
		JSONParser jsonParser = null;
		JSONObject jsonObject = null;
		try {
			jsonParser = new JSONParser();
			jsonObject = (JSONObject) jsonParser.parse(downloadUrl(url));
			JSONArray routes = (JSONArray) jsonObject.get("routes");
			JSONObject route = null;
			JSONArray legs = null;
			JSONObject leg = null;
			if (!routes.isEmpty()) {
				route = (JSONObject) routes.get(0);
				legs = (JSONArray) route.get("legs");
				leg = (JSONObject) legs.get(0);
				String distanceStr = (String) ((JSONObject) leg.get("distance")).get("text");
				//String durationStr = (String) ((JSONObject) leg.get("duration")).get("text");

				if (distanceStr.contains("mi")) {
					distanceStr = distanceStr.replace("mi", "").trim();
					distance = Double.valueOf(distanceStr);
				} else if (distanceStr.contains("ft")) {
					distanceStr = distanceStr.replace("ft", "").trim();
					distance = Integer.valueOf(distanceStr) / 5280;
					distance = round(distance, 1);
				}
				directionAPIResponse.setDistance(distance);
				//directionAPIResponse.setEta(durationStr);
				//System.out.println("durationStr"+durationStr);
				System.out.println("distance"+distance);


			}
		} catch (Exception e) {
			System.out.println("distance"+e.getMessage());

		}
		return directionAPIResponse;

	}

	private static String downloadUrl(String strUrl) {
		String data = "";
		InputStream iStream = null;
		HttpURLConnection urlConnection = null;
		try {
			URL url = new URL(strUrl);
			urlConnection = (HttpURLConnection) url.openConnection();
			urlConnection.connect();
			iStream = urlConnection.getInputStream();
			BufferedReader br = new BufferedReader(new InputStreamReader(iStream, "UTF-8"));
			StringBuilder sb = new StringBuilder();
			String line = "";
			while ((line = br.readLine()) != null) {
				sb.append(line);
			}
			data = sb.toString();
			br.close();
			iStream.close();
		} catch (IOException e) {
		} finally {
			if (urlConnection != null) {
				urlConnection.disconnect();
			}
		}
		return data;
	}

}
