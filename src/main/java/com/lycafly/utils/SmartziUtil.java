package com.lycafly.utils;

import com.lycafly.dto.response.SmartziResponse;

public class SmartziUtil {

	 /**
	 * Instantiates a new here util.
	 */
   private SmartziUtil() {
       // private constructor
   }

   /**
    * To generate a uniform response for every WS call.
    *
    * @param data data
    * @param message message
    * @param status status
    * @return LeapResponse leapresponse
    */
   public static SmartziResponse generateResponse(Object data, String message, String status) {
	   SmartziResponse hereResponse = new SmartziResponse();
   	hereResponse.setData(data);
   	hereResponse.setMessage(message);
   	hereResponse.setStatus(status);
       return hereResponse;
   }
}
