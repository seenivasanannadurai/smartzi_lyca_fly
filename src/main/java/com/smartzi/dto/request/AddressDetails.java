package com.smartzi.dto.request;

public class AddressDetails {

	private String sourceAddress;

	private String destinationAddress;

	public void setSourceAddress(String sourceAddress) {
		this.sourceAddress = sourceAddress;
	}

	public String getSourceAddress() {
		return sourceAddress;
	}

	public void setDestinationAddress(String destinationAddress) {
		this.destinationAddress=destinationAddress;

	}
	
	public String getDestinationAddress() {
		return destinationAddress;
	}

}
