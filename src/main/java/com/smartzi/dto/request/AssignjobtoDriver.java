package com.smartzi.dto.request;

public class AssignjobtoDriver {

	private Long tripRequestId;
	private Long driverId;

	private Boolean isMarkePrice;

	private Double adminPromoRate;

	private String referralCode;

	private String isAssign;

	private String adminNotes;

	private boolean controllerBookingTrip;

	public Long getTripRequestId() {
		return tripRequestId;
	}

	public void setTripRequestId(Long tripRequestId) {
		this.tripRequestId = tripRequestId;
	}

	public Long getDriverId() {
		return driverId;
	}

	public void setDriverId(Long driverId) {
		this.driverId = driverId;
	}

	public Boolean getIsMarkePrice() {
		return isMarkePrice;
	}

	public void setIsMarkePrice(Boolean isMarkePrice) {
		this.isMarkePrice = isMarkePrice;
	}

	public Double getAdminPromoRate() {
		return adminPromoRate;
	}

	public void setAdminPromoRate(Double adminPromoRate) {
		this.adminPromoRate = adminPromoRate;
	}

	public String getReferralCode() {
		return referralCode;
	}

	public void setReferralCode(String referralCode) {
		this.referralCode = referralCode;
	}

	public String getIsAssign() {
		return isAssign;
	}

	public void setIsAssign(String isAssign) {
		this.isAssign = isAssign;
	}

	public String getAdminNotes() {
		return adminNotes;
	}

	public void setAdminNotes(String adminNotes) {
		this.adminNotes = adminNotes;
	}

	public boolean isControllerBookingTrip() {
		return controllerBookingTrip;
	}

	public void setControllerBookingTrip(boolean controllerBookingTrip) {
		this.controllerBookingTrip = controllerBookingTrip;
	}

}
