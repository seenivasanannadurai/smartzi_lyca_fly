package com.smartzi.dto.request;

public class CustomerEmailsByMobNoReq {
	/**
     * 
     */
    private String mobileNo;
    
    /**
     * 
     */
    private String emailId;
    
    /**
     * 
     */
    private String firstName;
    
     /**
     * @return the mobileNo
     */
    public String getMobileNo() {
        return mobileNo;
    }

    /**
     * @param mobileNo
     *            the mobileNo to set
     */
    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
    

}
