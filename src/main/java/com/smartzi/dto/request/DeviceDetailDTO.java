package com.smartzi.dto.request;

public class DeviceDetailDTO {
	 /**
     * 
     */
    private Long deviceId;
    /**
     * 
     */
    private String deviceName;
    /**
     * 
     */
    private String deviceModel;
    /**
     * 
     */
    private short appPlatform;
    /**
     * 
     */
    private String appPlatformVersion;
    /**
     * 
     */
    private String deviceToken;
    /**
     * 
     */
    private String deviceLocale;
    /**
     * 
     */
    private short appType;

    /**
     * @return the deviceId
     */
    public Long getDeviceId() {
        return deviceId;
    }

    /**
     * @param deviceId
     *            the deviceId to set
     */
    public void setDeviceId(Long deviceId) {
        this.deviceId = deviceId;
    }

    /**
     * @return the deviceName
     */
    public String getDeviceName() {
        return deviceName;
    }

    /**
     * @param deviceName
     *            the deviceName to set
     */
    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    /**
     * @return the deviceModel
     */
    public String getDeviceModel() {
        return deviceModel;
    }

    /**
     * @param deviceModel
     *            the deviceModel to set
     */
    public void setDeviceModel(String deviceModel) {
        this.deviceModel = deviceModel;
    }

    /**
     * @return the appPlatform
     */
    public short getAppPlatform() {
        return appPlatform;
    }

    /**
     * @param appPlatform
     *            the appPlatform to set
     */
    public void setAppPlatform(short appPlatform) {
        this.appPlatform = appPlatform;
    }

    /**
     * @return the appPlatformVersion
     */
    public String getAppPlatformVersion() {
        return appPlatformVersion;
    }

    /**
     * @param appPlatformVersion
     *            the appPlatformVersion to set
     */
    public void setAppPlatformVersion(String appPlatformVersion) {
        this.appPlatformVersion = appPlatformVersion;
    }

    /**
     * @return the deviceToken
     */
    public String getDeviceToken() {
        return deviceToken;
    }

    /**
     * @param deviceToken
     *            the deviceToken to set
     */
    public void setDeviceToken(String deviceToken) {
        this.deviceToken = deviceToken;
    }

    /**
     * @return the deviceLocale
     */
    public String getDeviceLocale() {
        return deviceLocale;
    }

    /**
     * @param deviceLocale
     *            the deviceLocale to set
     */
    public void setDeviceLocale(String deviceLocale) {
        this.deviceLocale = deviceLocale;
    }

    /**
     * @return the appType
     */
    public short getAppType() {
        return appType;
    }

    /**
     * @param appType
     *            the appType to set
     */
    public void setAppType(short appType) {
        this.appType = appType;
    }

}
