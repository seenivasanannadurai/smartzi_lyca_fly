package com.smartzi.dto.request;

public class GetPreferDriverRequest {
	
	private Long customerId;

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

}
