package com.smartzi.dto.request;



/**
 * 
 * DTO to hold fetch trip reservation details request parameters
 * 
 */
public class GetTripReservDetailRequest {

    private Long tripRequestId;
    /**
     * 
     */
    private Long tripReservationId;

    /**
     * @return the tripReservationId
     */
    public Long getTripReservationId() {
        return tripReservationId;
    }

    /**
     * @param tripReservationId
     *            the tripReservationId to set
     */
    public void setTripReservationId(Long tripReservationId) {
        this.tripReservationId = tripReservationId;
    }

    /**
     * @return the tripRequestId
     */
    public Long getTripRequestId() {
        return tripRequestId;
    }

    /**
     * @param tripRequestId the tripRequestId to set
     */
    public void setTripRequestId(Long tripRequestId) {
        this.tripRequestId = tripRequestId;
    }
}

