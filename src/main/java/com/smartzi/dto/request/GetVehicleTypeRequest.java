package com.smartzi.dto.request;

public class GetVehicleTypeRequest {

    private short passengers_no;
    private short suitcases_no;

    public short getSuitcases_no() {
        return suitcases_no;
    }

    public void setSuitcases_no(short suitcases_no) {
        this.suitcases_no = suitcases_no;
    }

    public short getPassengers_no() {
        return passengers_no;
    }

    public void setPassengers_no(short passengers_no) {
        this.passengers_no = passengers_no;
    }
}
