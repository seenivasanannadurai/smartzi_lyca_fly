package com.smartzi.dto.request;

public class TaxiBookingRequest {
	
    /**
    *
    */
   private String firstName;

   /**
    *
    */
   private String lastName;

   /**
    *
    */
   private String countryCode;

   /**
    *
    */
   private String mobileNo;

   /**
    *
    */
   private String emailId;

   /**
    *
    */
   private TripEnquiryRequest tripEnquiryRequest;

   /**
    * @return the firstName
    */
   public String getFirstName() {
       return firstName;
   }

   /**
    * @param firstName
    *            the firstName to set
    */
   public void setFirstName(String firstName) {
       this.firstName = firstName;
   }

   /**
    * @return the lastName
    */
   public String getLastName() {
       return lastName;
   }

   /**
    * @param lastName
    *            the lastName to set
    */
   public void setLastName(String lastName) {
       this.lastName = lastName;
   }

   /**
    * @return the countryCode
    */
   public String getCountryCode() {
       return countryCode;
   }

   /**
    * @param countryCode
    *            the countryCode to set
    */
   public void setCountryCode(String countryCode) {
       this.countryCode = countryCode;
   }

   /**
    * @return the mobileNo
    */
   public String getMobileNo() {
       return mobileNo;
   }

   /**
    * @param mobileNo
    *            the mobileNo to set
    */
   public void setMobileNo(String mobileNo) {
       this.mobileNo = mobileNo;
   }

   /**
    * @return the emailId
    */
   public String getEmailId() {
       return emailId;
   }

   /**
    * @param emailId
    *            the emailId to set
    */
   public void setEmailId(String emailId) {
       this.emailId = emailId;
   }

   /**
    * @return the tripEnquiryRequest
    */
   public TripEnquiryRequest getTripEnquiryRequestTariff() {
       return tripEnquiryRequest;
   }

   /**
    * @param tripEnquiryRequestTariff
    *            the tripEnquiryRequest to set
    */
   public void setTripEnquiryRequestTariff(TripEnquiryRequest tripEnquiryRequest) {
       this.tripEnquiryRequest = tripEnquiryRequest;
   }


}
