package com.smartzi.dto.request;

public class TripEnquiryRequest {
	 /**
    *
    */
   private Long tripRequestId;
   /**
    *
    */
   private Double currentLocationLat;
   /**
    *
    */
   private Double currentLocationLong;
   /**
    *
    */
   private Double currentLocationAlt;
   /**
    *
    */
   private String sourceAddress;
   /**
    *
    */
   private Double sourceLat;
   /**
    *
    */
   private Double sourceLong;
   /**
    *
    */
   private String sourceTimeZone;
   /**
    *
    */
   private String destinationAddress;
   /**
    *
    */
   private Double destinationLat;
   /**
    *
    */
   private Double destinationLong;
   /**
    *
    */
   private Long customerId;
   /**
    *
    */
   private String tripReqDateTime;
   /**
    *
    */
   private String travelDateTime;
   /**
    *
    */
   private Double distance;
   /**
    *
    */
   private String estimatedTime;
   /**
    *
    */
   private String utcTravelDateTime;
   /**
    *
    */
   private String sourcePostCode;
   /**
    *
    */
   private String destPostCode;
   /**
    *
    */
   private String airportTrip;
   /**
    *
    */
   private String flightNumber;
   /**
    *
    */
   private String flightDatetime;
   /**
    *
    */
   private Integer passangerCount;
   /**
    *
    */
   private Integer lauggageCount;

   /**
    *
    */
   private String vehicleType;
   /**
    *
    */
   private String bookingNote;
   /**
    *
    */
   private double bookingAmount;
   /**
    *
    */
   private boolean customerNotification;
   /**
    *
    */
   private boolean driverNotification;
   /**
    *
    */
   private boolean controllerBookingTrip;
   /**
    *
    *
    */
   private String bookingMode;
   /**
    *
    *
    */
   private String stripePayToken;
   /**
    *
    */
   private Short paymentTypeId;
   /**
    *
    */
   private Boolean recurringPaymentAllowed;
   /**
    *
    */
   private String referralCode;
   /**
    *
    *
    */
   private String accountName;
   /**
    *
    *
    */
   private String passengerName;
   /**
    *
    *
    */
   private String passengerNo;
   /**
    *
    *
    */
   private double driverPrice;
   /**
    *
    *
    */
   private String adminNotes;

   /**
    *
    *
    */
   private Integer bookingSource;
   /**
    *
    *
    */
   private Integer bookingSourceType;
   /**
    *
    *
    */
   private Integer bookingSelect;
   /**
    *
    *
    */
   private Integer appPlatFormID;


   public Integer getBookingSource() {
       return bookingSource;
   }

   public void setBookingSource(Integer bookingSource) {
       this.bookingSource = bookingSource;
   }

   public Integer getBookingSourceType() {
       return bookingSourceType;
   }

   public void setBookingSourceType(Integer bookingSourceType) {
       this.bookingSourceType = bookingSourceType;
   }

   public Integer getBookingSelect() {
       return bookingSelect;
   }

   public void setBookingSelect(Integer bookingSelect) {
       this.bookingSelect = bookingSelect;
   }

   public Integer getAppPlatFormID() {
       return appPlatFormID;
   }

   public void setAppPlatFormID(Integer appPlatFormID) {
       this.appPlatFormID = appPlatFormID;
   }

   public String getVehicleType() {
       return vehicleType;
   }

   /**
    *
    */
   public void setVehicleType(String vehicleType) {
       this.vehicleType = vehicleType;
   }

   /**
    * @return the airportTrip
    */
   public String getAirportTrip() {
       return airportTrip;
   }

   /**
    * @param airportTrip
    *            the airportTrip to set
    */
   public void setAirportTrip(String airportTrip) {
       this.airportTrip = airportTrip;
   }


   /**
    * @return the flightNumber
    */
   public String getFlightNumber() {
       return flightNumber;
   }

   /**
    * @param flightNumber
    *            the flightNumber to set
    */
   public void setFlightNumber(String flightNumber) {
       this.flightNumber = flightNumber;
   }

   /**
    * @return the passangerCount
    */
   public Integer getPassangerCount() {
       return passangerCount;
   }

   /**
    * @param passangerCount
    *            the passangerCount to set
    */
   public void setPassangerCount(Integer passangerCount) {
       this.passangerCount = passangerCount;
   }

   /**
    * @return the lauggageCount
    */
   public Integer getLauggageCount() {
       return lauggageCount;
   }

   /**
    * @param lauggageCount
    *            the lauggageCount to set
    */
   public void setLauggageCount(Integer lauggageCount) {
       this.lauggageCount = lauggageCount;
   }


   /**
    * @return the flightDatetime
    */
   public String getFlightDatetime() {
       return flightDatetime;
   }

   /**
    * @param flightDatetime
    *            the flightDatetime to set
    */
   public void setFlightDatetime(String flightDatetime) {
       this.flightDatetime = flightDatetime;
   }

   /**
    * @return the tripRequestId
    */
   public Long getTripRequestId() {
       return tripRequestId;
   }

   /**
    * @param tripRequestId
    *            the tripRequestId to set
    */
   public void setTripRequestId(Long tripRequestId) {
       this.tripRequestId = tripRequestId;
   }

   /**
    * @return the currentLocationLat
    */
   public Double getCurrentLocationLat() {
       return currentLocationLat;
   }

   /**
    * @param currentLocationLat
    *            the currentLocationLat to set
    */
   public void setCurrentLocationLat(Double currentLocationLat) {
       this.currentLocationLat = currentLocationLat;
   }

   /**
    * @return the currentLocationLong
    */
   public Double getCurrentLocationLong() {
       return currentLocationLong;
   }

   /**
    * @param currentLocationLong
    *            the currentLocationLong to set
    */
   public void setCurrentLocationLong(Double currentLocationLong) {
       this.currentLocationLong = currentLocationLong;
   }

   /**
    * @return the currentLocationAlt
    */
   public Double getCurrentLocationAlt() {
       return currentLocationAlt;
   }

   /**
    * @param currentLocationAlt
    *            the currentLocationAlt to set
    */
   public void setCurrentLocationAlt(Double currentLocationAlt) {
       this.currentLocationAlt = currentLocationAlt;
   }

   /**
    * @return the sourceAddress
    */
   public String getSourceAddress() {
       return sourceAddress;
   }

   /**
    * @param sourceAddress
    *            the sourceAddress to set
    */
   public void setSourceAddress(String sourceAddress) {
       this.sourceAddress = sourceAddress;
   }

   /**
    * @return the sourceLat
    */
   public Double getSourceLat() {
       return sourceLat;
   }

   /**
    * @param sourceLat
    *            the sourceLat to set
    */
   public void setSourceLat(Double sourceLat) {
       this.sourceLat = sourceLat;
   }

   /**
    * @return the sourceLong
    */
   public Double getSourceLong() {
       return sourceLong;
   }

   /**
    * @param sourceLong
    *            the sourceLong to set
    */
   public void setSourceLong(Double sourceLong) {
       this.sourceLong = sourceLong;
   }

   /**
    * @return the sourceTimeZone
    */
   public String getSourceTimeZone() {
       return sourceTimeZone;
   }

   /**
    * @param sourceTimeZone
    *            the sourceTimeZone to set
    */
   public void setSourceTimeZone(String sourceTimeZone) {
       this.sourceTimeZone = sourceTimeZone;
   }

   /**
    * @return the destinationAddress
    */
   public String getDestinationAddress() {
       return destinationAddress;
   }

   /**
    * @param destinationAddress
    *            the destinationAddress to set
    */
   public void setDestinationAddress(String destinationAddress) {
       this.destinationAddress = destinationAddress;
   }

   /**
    * @return the destinationLat
    */
   public Double getDestinationLat() {
       return destinationLat;
   }

   /**
    * @param destinationLat
    *            the destinationLat to set
    */
   public void setDestinationLat(Double destinationLat) {
       this.destinationLat = destinationLat;
   }

   /**
    * @return the destinationLong
    */
   public Double getDestinationLong() {
       return destinationLong;
   }

   /**
    * @param destinationLong
    *            the destinationLong to set
    */
   public void setDestinationLong(Double destinationLong) {
       this.destinationLong = destinationLong;
   }

   /**
    * @return the customerId
    */
   public Long getCustomerId() {
       return customerId;
   }

   /**
    * @param customerId
    *            the customerId to set
    */
   public void setCustomerId(Long customerId) {
       this.customerId = customerId;
   }

   /**
    * @return the tripReqDateTime
    */
   public String getTripReqDateTime() {
       return tripReqDateTime;
   }

   /**
    * @param tripReqDateTime
    *            the tripReqDateTime to set
    */
   public void setTripReqDateTime(String tripReqDateTime) {
       this.tripReqDateTime = tripReqDateTime;
   }

   /**
    * @return the travelDateTime
    */
   public String getTravelDateTime() {
       return travelDateTime;
   }

   /**
    * @param travelDateTime
    *            the travelDateTime to set
    */
   public void setTravelDateTime(String travelDateTime) {
       this.travelDateTime = travelDateTime;
   }

   /**
    * @return the distance
    */
   public Double getDistance() {
       return distance;
   }

   /**
    * @param distance
    *            the distance to set
    */
   public void setDistance(Double distance) {
       this.distance = distance;
   }

   /**
    * @return the estimatedTime
    */
   public String getEstimatedTime() {
       return estimatedTime;
   }

   /**
    * @param estimatedTime
    *            the estimatedTime to set
    */
   public void setEstimatedTime(String estimatedTime) {
       this.estimatedTime = estimatedTime;
   }

   /**
    * @return the utcTravelDateTime
    */
   public String getUtcTravelDateTime() {
       return utcTravelDateTime;
   }

   /**
    * @param utcTravelDateTime
    *            the utcTravelDateTime to set
    */
   public void setUtcTravelDateTime(String utcTravelDateTime) {
       this.utcTravelDateTime = utcTravelDateTime;
   }

   /**
    * @return the sourcePostCode
    */
   public String getSourcePostCode() {
       return sourcePostCode;
   }

   /**
    * @param sourcePostCode
    *            the sourcePostCode to set
    */
   public void setSourcePostCode(String sourcePostCode) {
       this.sourcePostCode = sourcePostCode;
   }

   /**
    * @return the destPostCode
    */
   public String getDestPostCode() {
       return destPostCode;
   }

   /**
    * @param destPostCode
    *            the destPostCode to set
    */
   public void setDestPostCode(String destPostCode) {
       this.destPostCode = destPostCode;
   }

   public String getBookingNote() {
       return bookingNote;
   }

   public void setBookingNote(String bookingNote) {
       this.bookingNote = bookingNote;
   }

   public double getBookingAmount() {
       return bookingAmount;
   }

   public void setBookingAmount(double bookingAmount) {
       this.bookingAmount = bookingAmount;
   }

   public boolean isCustomerNotification() {
       return customerNotification;
   }

   public void setCustomerNotification(boolean customerNotification) {
       this.customerNotification = customerNotification;
   }

   public boolean isDriverNotification() {
       return driverNotification;
   }

   public void setDriverNotification(boolean driverNotification) {
       this.driverNotification = driverNotification;
   }

   public boolean isControllerBookingTrip() {
       return controllerBookingTrip;
   }

   public void setControllerBookingTrip(boolean controllerBookingTrip) {
       this.controllerBookingTrip = controllerBookingTrip;
   }

   public String getBookingMode() {
       return bookingMode;
   }

   public void setBookingMode(String bookingMode) {
       this.bookingMode = bookingMode;
   }

   public String getStripePayToken() {
       return stripePayToken;
   }

   public void setStripePayToken(String stripePayToken) {
       this.stripePayToken = stripePayToken;
   }

   public Short getPaymentTypeId() {
       return paymentTypeId;
   }

   public void setPaymentTypeId(Short paymentTypeId) {
       this.paymentTypeId = paymentTypeId;
   }

   public Boolean getRecurringPaymentAllowed() {
       return recurringPaymentAllowed;
   }

   public void setRecurringPaymentAllowed(Boolean recurringPaymentAllowed) {
       this.recurringPaymentAllowed = recurringPaymentAllowed;
   }

   public String getReferralCode() {
       return referralCode;
   }

   public void setReferralCode(String referralCode) {
       this.referralCode = referralCode;
   }

   public String getAccountName() {
       return accountName;
   }

   public void setAccountName(String accountName) {
       this.accountName = accountName;
   }

   public String getPassengerName() {
       return passengerName;
   }

   public void setPassengerName(String passengerName) {
       this.passengerName = passengerName;
   }

   public String getPassengerNo() {
       return passengerNo;
   }

   public void setPassengerNo(String passengerNo) {
       this.passengerNo = passengerNo;
   }

   public double getDriverPrice() {
       return driverPrice;
   }

   public void setDriverPrice(double driverPrice) {
       this.driverPrice = driverPrice;
   }

   /**
    * @return the adminNotes
    */
   public String getAdminNotes() {
       return adminNotes;
   }

   /**
    * @param adminNotes the adminNotes to set
    */
   public void setAdminNotes(String adminNotes) {
       this.adminNotes = adminNotes;
   }

}


