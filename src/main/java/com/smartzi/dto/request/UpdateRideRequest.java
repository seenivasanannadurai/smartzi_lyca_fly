package com.smartzi.dto.request;

import com.lycafly.dto.request.Destination;
import com.lycafly.dto.request.Operator;
import com.lycafly.dto.request.Pickup;
import com.lycafly.dto.request.PickupTime;

public class UpdateRideRequest {

	private String supplierId;

	private String flightBookReference;

	private Pickup pickup;

	private Destination destination;

	private Passenger passenger;

	private PickupTime pickupTime;

	private String vehicleType;

	private Operator operator;

	private Long rideId;

	public Long getRideId() {
		return rideId;
	}

	public void setRideId(Long rideId) {
		this.rideId = rideId;
	}

	public String getFlightBookReference() {
		return flightBookReference;
	}

	public void setFlightBookReference(String flightBookReference) {
		this.flightBookReference = flightBookReference;
	}

	public Operator getOperator() {
		return operator;
	}

	public void setOperator(Operator operator) {
		this.operator = operator;
	}

	public String getSupplierId() {
		return supplierId;
	}

	public void setSupplierId(String supplierId) {
		this.supplierId = supplierId;
	}

	public Pickup getPickup() {
		return pickup;
	}

	public void setPickup(Pickup pickup) {
		this.pickup = pickup;
	}

	public Destination getDestination() {
		return destination;
	}

	public void setDestination(Destination destination) {
		this.destination = destination;
	}

	public Passenger getPassenger() {
		return passenger;
	}

	public void setPassenger(Passenger passenger) {
		this.passenger = passenger;
	}

	public String getPassengerNote() {
		return passengerNote;
	}

	public void setPassengerNote(String passengerNote) {
		this.passengerNote = passengerNote;
	}

	public String getVehicleType() {
		return vehicleType;
	}

	public void setVehicleType(String vehicleType) {
		this.vehicleType = vehicleType;
	}

	public PickupTime getPickupTime() {
		return pickupTime;
	}

	public void setPickupTime(PickupTime pickupTime) {
		this.pickupTime = pickupTime;
	}

	private String passengerNote;

}
