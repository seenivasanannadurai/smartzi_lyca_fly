package com.smartzi.dto.request;



public class UpdateTripReservStatusRequest {
    /**
     * 
     */
    private Long tripReservationId;
    /**
     *
     */
    private Integer tripReservationStatus;
    /**
     *
     */
    private String cancelReason;
    /**
     *
     */
    private String startTime;
    /**
     *
     */
    private Double currentLat;
    /**
     *
     */
    private Double currentLong;
    
    /**
     * 
     */
    private boolean reassignJob;

    /**
     * @return the tripReservationId
     */
    public Long getTripReservationId() {
        return tripReservationId;
    }

    /**
     * @param tripReservationId
     *            the tripReservationId to set
     */
    public void setTripReservationId(Long tripReservationId) {
        this.tripReservationId = tripReservationId;
    }

    /**
     * @return the tripReservationStatus
     */
    public Integer getTripReservationStatus() {
        return tripReservationStatus;
    }

    /**
     * @param tripReservationStatus
     *            the tripReservationStatus to set
     */
    public void setTripReservationStatus(Integer tripReservationStatus) {
        this.tripReservationStatus = tripReservationStatus;
    }

    /**
     * @return the cancelReason
     */
    public String getCancelReason() {
        return cancelReason;
    }

    /**
     * @param cancelReason
     *            the cancelReason to set
     */
    public void setCancelReason(String cancelReason) {
        this.cancelReason = cancelReason;
    }

    /**
     * @return the startTime
     */
    public String getStartTime() {
        return startTime;
    }

    /**
     * @param startTime
     *            the startTime to set
     */
    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    /**
     * @return the currentLat
     */
    public Double getCurrentLat() {
        return currentLat;
    }

    /**
     * @param currentLat
     *            the currentLat to set
     */
    public void setCurrentLat(Double currentLat) {
        this.currentLat = currentLat;
    }

    /**
     * @return the currentLong
     */
    public Double getCurrentLong() {
        return currentLong;
    }

    /**
     * @param currentLong
     *            the currentLong to set
     */
    public void setCurrentLong(Double currentLong) {
        this.currentLong = currentLong;
    }

	public boolean isReassignJob() {
		return reassignJob;
	}

	public void setReassignJob(boolean reassignJob) {
		this.reassignJob = reassignJob;
	}
    
    

}

