package com.smartzi.dto.request;

public class UserRegistrationRequest {
	/**
     * 
     */
    private Long userId;
    /**
     * 
     */
    private String firstName;
    /**
     * 
     */
    private String lastName;
    /**
     * 
     */
    private String countryCode;
    /**
     * 
     */
    private String mobileNo;
    /**
     * 
     */
    private String emailId;
    /**
     * 
     */
    private String password;
    /**
     * 
     */
    private short userType;
    /**
     * 
     */
    private DeviceDetailDTO deviceDetail;
    
    /**
     * 
     */
    private String referralCode;

    /**
     * @return the userId
     */
    public Long getUserId() {
        return userId;
    }

    /**
     * @param userId
     *            the userId to set
     */
    public void setUserId(Long userId) {
        this.userId = userId;
    }

    /**
     * @return the firstName
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * @param firstName
     *            the firstName to set
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * @return the lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * @param lastName
     *            the lastName to set
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * @return the countryCode
     */
    public String getCountryCode() {
        return countryCode;
    }

    /**
     * @param countryCode
     *            the countryCode to set
     */
    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    /**
     * @return the mobileNo
     */
    public String getMobileNo() {
        return mobileNo;
    }

    /**
     * @param mobileNo
     *            the mobileNo to set
     */
    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    /**
     * @return the emailId
     */
    public String getEmailId() {
        return emailId;
    }

    /**
     * @param emailId
     *            the emailId to set
     */
    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password
     *            the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the userType
     */
    public short getUserType() {
        return userType;
    }

    /**
     * @param userType
     *            the userType to set
     */
    public void setUserType(short userType) {
        this.userType = userType;
    }

    /**
     * @return the deviceDetail
     */
    public DeviceDetailDTO getDeviceDetail() {
        return deviceDetail;
    }

    /**
     * @param deviceDetail
     *            the deviceDetail to set
     */
    public void setDeviceDetail(DeviceDetailDTO deviceDetail) {
        this.deviceDetail = deviceDetail;
    }
    /**
     * @return the referralCode
     */
    public String getReferralCode() {
        return referralCode;
    }

    /**
     * @param referralCode
     *            the referralCode to set
     */
    public void setReferralCode(String referralCode) {
        this.referralCode = referralCode;
    }
}



