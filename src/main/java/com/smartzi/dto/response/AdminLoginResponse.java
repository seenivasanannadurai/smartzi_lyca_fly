package com.smartzi.dto.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
// TODO: Auto-generated Javadoc

/**
 * The Class AdminLoginResponse.
 */
public class AdminLoginResponse {
    
    /** The o auth response. */
    private OAuthResponse oAuthResponse;

    /** The user GUID. */
    private String userGUID;

    /** The user id. */
    private Long userId;

    /** The user name. */
    private String userName;

    /** The email id. */
    private String emailId;
    
    /** The referral code. */
    private String referralCode;

    /**
     * Gets the referral code.
     *
     * @return the referral code
     */
    public String getReferralCode() {
        return referralCode;
    }

    /**
     * Sets the referral code.
     *
     * @param referralCode the new referral code
     */
    public void setReferralCode(String referralCode) {
        this.referralCode = referralCode;
    }

    /**
     * The user type.
     *
     * @return the oAuthResponse
     */

    private short userType;

    /**
     * Gets the o auth response.
     *
     * @return the o auth response
     */
    public OAuthResponse getoAuthResponse() {
        return oAuthResponse;
    }

    /**
     * Sets the o auth response.
     *
     * @param oAuthResponse            the oAuthResponse to set
     */
    public void setoAuthResponse(OAuthResponse oAuthResponse) {
        this.oAuthResponse = oAuthResponse;
    }

    /**
     * Gets the user GUID.
     *
     * @return the userGUID
     */
    public String getUserGUID() {
        return userGUID;
    }

    /**
     * Sets the user GUID.
     *
     * @param userGUID            the userGUID to set
     */
    public void setUserGUID(String userGUID) {
        this.userGUID = userGUID;
    }

    /**
     * Gets the user id.
     *
     * @return the userId
     */
    public Long getUserId() {
        return userId;
    }

    /**
     * Sets the user id.
     *
     * @param userId            the userId to set
     */
    public void setUserId(Long userId) {
        this.userId = userId;
    }

    /**
     * Gets the user name.
     *
     * @return the userName
     */
    public String getUserName() {
        return userName;
    }

    /**
     * Sets the user name.
     *
     * @param userName            the userName to set
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * Gets the email id.
     *
     * @return the emailId
     */
    public String getEmailId() {
        return emailId;
    }

    /**
     * Sets the email id.
     *
     * @param emailId            the emailId to set
     */
    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    /**
     * Gets the user type.
     *
     * @return the user type
     */
    public short getuserType() {
        return userType;
    }

    /**
     * Sets the user type.
     *
     * @param userType            the userType to set
     */
    public void setuserType(short userType) {
        this.userType = userType;
    }
}

