package com.smartzi.dto.response;

public class BookRideResponse {
	private Long rideId;
	 
	public Long getRideId() {
		return rideId;
	}

	public void setRideId(Long rideId) {
		this.rideId = rideId;
	}

	public String getBookReference() {
		return bookReference;
	}

	public void setBookReference(String bookReference) {
		this.bookReference = bookReference;
	}

	private String bookReference;

}
