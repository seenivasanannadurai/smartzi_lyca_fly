package com.smartzi.dto.response;

public class CustomerEmailsByMobNoResp {
	/**
	* 
	*/
	private Long customerId;
	/**
	 * 
	 */
	private String emailId;
	/**
	 * 
	 */
	private String firstName;
	/**
	 * 
	 */
	private String lastName;

	/**
	 * 
	 */
	private boolean cardDetail;
	/**
	 * 
	 */
	private String mobileNo;

	/**
	 * @return the customerId
	 */
	public Long getCustomerId() {
		return customerId;
	}

	/**
	 * @param customerId
	 *            the customerId to set
	 */
	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	/**
	 * @return the emailId
	 */
	public String getEmailId() {
		return emailId;
	}

	/**
	 * @param emailId
	 *            the emailId to set
	 */
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName
	 *            the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName
	 *            the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public boolean isCardDetail() {
		return cardDetail;
	}

	public void setCardDetail(boolean cardDetail) {
		this.cardDetail = cardDetail;
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

}
