package com.smartzi.dto.response;

public class CustomerRegistrationResponse {
	  /**
     * 
     */
    private String userGUID;

    /**
     * 
     */
    private Long customerId;

    /**
     * @return the userGUID
     */
    public String getUserGUID() {
        return userGUID;
    }

    /**
     * @param userGUID
     *            the userGUID to set
     */
    public void setUserGUID(String userGUID) {
        this.userGUID = userGUID;
    }
    
    

    /**
     * @return the customerId
     */
    public Long getCustomerId() {
        return customerId;
    }

    /**
     * @param customerId
     *            the customerId to set
     */
    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

}
