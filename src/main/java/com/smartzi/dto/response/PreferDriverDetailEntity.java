package com.smartzi.dto.response;

import java.sql.Timestamp;

public class PreferDriverDetailEntity {


    private Long driverId;

    private String driverCode;

    private Long customerId;

    private Long preferDriverId;

    private Long updatedBy;

    private Timestamp createdOn;

    private Timestamp updatedOn;

    /**
     * @return the driverId
     */
    public Long getDriverId() {
        return driverId;
    }

    /**
     * @param driverId the driverId to set
     */
    public void setDriverId(Long driverId) {
        this.driverId = driverId;
    }


    /**
     * @return the driverId
     */
    public String getdriverCode() {
        return driverCode;
    }

    /**
     * @param driverCode the driverId to set
     */
    public void setdriverCode(String driverCode) {
        this.driverCode = driverCode;
    }

    /**
     * @return the customerId
     */
    public Long getCustomerId() {
        return customerId;
    }

    /**
     * @param customerId the customerId to set
     */
    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    /**
     * /**
     *
     * @return the updatedBy
     */
    public Long getUpdatedBy() {
        return updatedBy;
    }

    /**
     * @param updatedBy the updatedBy to set
     */
    public void setUpdatedBy(Long updatedBy) {
        this.updatedBy = updatedBy;
    }

    /**
     * @return the createdOn
     */
    public Timestamp getCreatedOn() {
        return createdOn;
    }

    /**
     * @param createdOn the createdOn to set
     */
    public void setCreatedOn(Timestamp createdOn) {
        this.createdOn = createdOn;
    }

    /**
     * @return the updatedOn
     */
    public Timestamp getUpdatedOn() {
        return updatedOn;
    }

    /**
     * @param updatedOn the updatedOn to set
     */
    public void setUpdatedOn(Timestamp updatedOn) {
        this.updatedOn = updatedOn;
    }


    public Long getPreferDriverId() {
        return preferDriverId;
    }

    /**
     * @param PreferDriverId the PreferDriverId to set
     */
    public void setPreferDriverId(Long PreferDriverId) {
        this.preferDriverId = PreferDriverId;
    }
}
