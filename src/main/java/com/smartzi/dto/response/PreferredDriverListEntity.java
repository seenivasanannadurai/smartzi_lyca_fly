package com.smartzi.dto.response;


public class PreferredDriverListEntity {

    private Long userId;

    private String firstName;

    private String lastName;

    private Short status;

    private Short userType;

    private Long driverId;

    private String imagePath;

    private String driverCode;

    private String fromTime;

    private String toTime;

    private String mobileNo;

    private Double ratePerMile;

    private Long preferDriverID;

    /**
     * @return the userDetailId
     */
    public String getdriverCode() {
        return driverCode;
    }

    /**
     * @param driverCode the userDetailId to set
     */
    public void setdriverCode(String driverCode) {
        this.driverCode = driverCode;
    }


    /**
     * @return the userId
     */
    public Long getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(Long userId) {
        this.userId = userId;
    }

    /**
     * @return the firstName
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * @param firstName the firstName to set
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * @return the lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * @param lastName the lastName to set
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }


    /**
     * @return the status
     */
    public Short getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(Short status) {
        this.status = status;
    }

    /**
     * @return the userType
     */
    public Short getUserType() {
        return userType;
    }

    /**
     * @param userType the userType to set
     */
    public void setUserType(Short userType) {
        this.userType = userType;
    }


    /**
     * @return the driverId
     */
    public Long getDriverId() {
        return driverId;
    }

    /**
     * @param driverId the driverId to set
     */
    public void setDriverId(Long driverId) {
        this.driverId = driverId;
    }


    /**
     * @return the imagePath
     */
    public String getImagePath() {
        return imagePath;
    }

    /**
     * @param imagePath the imagePath to set
     */
    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    /**
     * @return the mimeType
     */

    public Long getPreferDriverID() {
        return preferDriverID;
    }

    /**
     * @param PreferDriverID the driverEarning to set
     */
    public void setPreferDriverID(Long PreferDriverID) {
        this.preferDriverID = PreferDriverID;
    }

    public String getfromTime() {
        return fromTime;
    }

    /**
     * @param fromTime the driverEarning to set
     */
    public void setfromTime(String fromTime) {
        this.fromTime = fromTime;
    }

    public String gettoTime() {
        return toTime;
    }

    /**
     * @param toTime the driverEarning to set
     */
    public void settoTime(String toTime) {
        this.toTime = toTime;
    }

    /**
     * public String getmobileNoe() {
     * return mobileNo;
     * }
     * <p>
     * /**
     *
     * @param mobileNo the driverEarning to set
     */
    public void setmobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public Double getRatePerMile() {
        return ratePerMile;
    }

    /**
     * @param ratePerMile the driverEarning to set
     */
    public void setRatePerMile(Double ratePerMile) {
        this.ratePerMile = ratePerMile;
    }
}
