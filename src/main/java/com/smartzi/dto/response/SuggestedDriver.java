package com.smartzi.dto.response;

public class SuggestedDriver {/**
     * 
     */
    private Long driverId;
    /**
     *
     */
    private Double estimatedFare;
    /**
     *
     */
    private Integer eta;
    /**
     *
     */
    private Double ratePerMile;
    /**
     *
     */
    private Double minRate;
    /**
     *
     */
    private Boolean checkAvailability;
    /**
     * 
     */
    private Integer numberOfSeats;
    /**
     * 
     */
	private Double latitude;
	 /**
     * 
     */
	private Double longitude;
	 /**
     * 
     */
	private String vehicleType;
	/**
     * 
     */
    private boolean isWheelChairAvailable;
    /**
     * 
     */
    private String isCrossDispatchON;
    /**
     * 
     */
    private String isHighlight;
    
    /**
     * @return the isHighlight
     */
    public String getIsHighlight() {
        return isHighlight;
    }

    /**
     * @param isHighlight
     *            the isHighlight to set
     */
    public void setIsHighlight(String isHighlight) {
        this.isHighlight = isHighlight;
    }
    
    /**
     * @return the isCrossDispatchON
     */
    public String getIsCrossDispatchON() {
        return isCrossDispatchON;
    }

    /**
     * @param isCrossDispatchON
     *            the isCrossDispatchON to set
     */
    public void setIsCrossDispatchON(String isCrossDispatchON) {
        this.isCrossDispatchON = isCrossDispatchON;
    }
   
	 /**
     * 
     */
    public boolean isLuxuryVehicleAvailable() {
        return isWheelChairAvailable;
    }
    
    /**
     * @return the isLuxuryVehicleAvailable
     */
    public Boolean getIsLuxuryVehicleAvailable() {
        return isWheelChairAvailable;
    }


    /**
     * 
     */
    public void setLuxuryVehicleAvailable(boolean isWheelChairAvailable) {
        this.isWheelChairAvailable = isWheelChairAvailable;
    }
    public String getVehicleType() {
        return vehicleType;
    }

    /**
     * 
     */
    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }
    
    /**
     * @return the numberOfSeats
     */
    public Integer getNumberOfSeats() {
        return numberOfSeats;
    }

    /**
     * @param numberOfSeats
     *            the numberOfSeats to set
     */
    public void setNumberOfSeats(Integer numberOfSeats) {
        this.numberOfSeats = numberOfSeats;
    }

    /**
     * @return the driverId
     */
    public Long getDriverId() {
        return driverId;
    }

    /**
     * @param driverId
     *            the driverId to set
     */
    public void setDriverId(Long driverId) {
        this.driverId = driverId;
    }

    /**
     * @return the estimatedFare
     */
    public Double getEstimatedFare() {
        return estimatedFare;
    }

    /**
     * @param estimatedFare
     *            the estimatedFare to set
     */
    public void setEstimatedFare(Double estimatedFare) {
        this.estimatedFare = estimatedFare;
    }

    /**
     * @return the eta
     */
    public Integer geteTA() {
        return eta;
    }

    /**
     * @param eta
     *            the eta to set
     */
    public void seteTA(Integer eta) {
        this.eta = eta;
    }

    /**
     * @return the ratePerMile
     */
    public Double getRatePerMile() {
        return ratePerMile;
    }

    /**
     * @param ratePerMile
     *            the ratePerMile to set
     */
    public void setRatePerMile(Double ratePerMile) {
        this.ratePerMile = ratePerMile;
    }

    /**
     * @return the minRate
     */
    public Double getMinRate() {
        return minRate;
    }

    /**
     * @param minRate
     *            the minRate to set
     */
    public void setMinRate(Double minRate) {
        this.minRate = minRate;
    }

    /**
     * @return the checkAvailability
     */
    public Boolean getCheckAvailability() {
        return checkAvailability;
    }

    /**
     * @param checkAvailability
     *            the checkAvailability to set
     */
    public void setCheckAvailability(Boolean checkAvailability) {
        this.checkAvailability = checkAvailability;
    }

	public void setLatitude(Double latitude) {
		
		this.latitude = latitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}
	
	public Double getLatitude() {
		return latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

}
