package com.smartzi.dto.response;

import java.util.List;

public class TripEnquiryResponse {
	  /**
    *
    */
   private Boolean isDriverAcceptence;
   /**
    *
    */
   private Integer waitingTime;
   /**
    *
    */
   private Integer retryTripCount;
   /**
    * 
    */
   private int tripType;
   /**
    * 
    */
   private Long TripRequestId;
   /**
    * 
    */
   private List<SuggestedDriver> suggestedDrivers;
   /**
    * 
    */
   private List<PreferDriverDetailEntity> preferDrivers;
   /**
    * 
    */
   private String bookingId;
   /**
    * 
    */
   private Double ratePerMile;
   /**
    * 
    */
   private Long customerId;
   
   /**
  	 * 
  	 */
      private String airportTrip;
      /**
  	 * 
  	 */
      private String flightNumber;
      /**
  	 * 
  	 */
      private String flightDatetime;
      /**
     	 * 
     	 */
      private Integer passangerCount;
      /**
     	 * 
     	 */
      private Integer lauggageCount;
         
      /**
     	 * 
     	 */
         private String airportPromoApplied;
         

   /**
    *
    */
   private double bookingAmount;
   /**
    *
    */
   private String bookingMode;
   /**
    *
    */
   private String referralCode;
   /**
    * 
    */
   private String vehicleType;
      /**
       * @return the airportTrip
       */
   /**
    * 
    */
   private Long TripReservationId;
   
   /**
    * 
    * @return
    */
   private Long driverId;
   /**
    * 
    * @return
    */
  
   private String selfDriverAccountName;
   /**
   *
   */
  private String sourceAddress;
  /**
  *
  */
  private String destinationAddress;
  /**
  *
  */
 private String utcTravelDateTime;

   
   public String getAirportTrip() {
          return airportTrip;
      }

      /**
       * @param airportTrip
       *            the airportTrip to set
       */
      public void setAirportTrip(String airportTrip) {
          this.airportTrip = airportTrip;
      }

      
      /**
       * @return the flightNumber
       */
      public String getFlightNumber() {
          return flightNumber;
      }

      /**
       * @param flightNumber
       *            the flightNumber to set
       */
      public void setFlightNumber(String flightNumber) {
          this.flightNumber = flightNumber;
      }
      
      /**
       * @return the passangerCount
       */
      public Integer getPassangerCount() {
          return passangerCount;
      }

      /**
       * @param passangerCount
       *            the passangerCount to set
       */
      public void setPassangerCount(Integer passangerCount) {
          this.passangerCount = passangerCount;
      }
      
      /**
       * @return the lauggageCount
       */
      public Integer getLauggageCount() {
          return lauggageCount;
      }

      /**
       * @param lauggageCount
       *            the lauggageCount to set
       */
      public void setLauggageCount(Integer lauggageCount) {
          this.lauggageCount = lauggageCount;
      }
      
      
      /**
       * @return the flightDatetime
       */
      public String getFlightDatetime() {
          return flightDatetime;
      }

      /**
       * @param flightDatetime
       *            the flightDatetime to set
       */
      public void setFlightDatetime(String flightDatetime) {
          this.flightDatetime = flightDatetime;
      }
      
      
      
   
   /**
    * 
    */
   public Double getRatePerMile() {
       return ratePerMile;
   }

   /**
    * 
    */
   public void setRatePerMile(Double ratePerMile) {
       this.ratePerMile = ratePerMile;
   }

   /**
    * 
    */
   public int getTripType() {
       return tripType;
   }

   /**
    * 
    */
   public void setTripType(int tripType) {
       this.tripType = tripType;
   }

   /**
    * 
    */
   public Long getTripRequestId() {
       return TripRequestId;
   }

   /**
    * 
    */
   public void setTripRequestId(Long tripRequestId) {
       TripRequestId = tripRequestId;
   }

   /**
    * 
    */
   public List<SuggestedDriver> getSuggestedDrivers() {
       return suggestedDrivers;
   }

   /**
    * 
    */
   public void setSuggestedDrivers(List<SuggestedDriver> suggestedDrivers) {
       this.suggestedDrivers = suggestedDrivers;
   }
   
   
   /**
    * 
    */
   public List<PreferDriverDetailEntity> getPreferDrivers() {
       return preferDrivers;
   }

   /**
    * 
    */
   public void setPreferDrivers(List<PreferDriverDetailEntity> preferDrivers) {
       this.preferDrivers = preferDrivers;
   }

   /**
    * 
    */
   public String getBookingId() {
       return bookingId;
   }

   /**
    * 
    */
   public void setBookingId(String bookingId) {
       this.bookingId = bookingId;
   }

   /**
    * 
    */
   public Long getCustomerId() {
       return customerId;
   }

   /**
    * 
    */
   public void setCustomerId(Long customerId) {
       this.customerId = customerId;
   }
   
   /**
    * @return the airportPromoApplied
    */
   public String getAirportPromoApplied() {
       return airportPromoApplied;
   }

   /**
    * @param airportPromoApplied
    *            the airportPromoApplied to set
    */
   public void setAirportPromoApplied(String airportPromoApplied) {
       this.airportPromoApplied = airportPromoApplied;
   }

   /**
    * @param isDriverAcceptence the active to set
    */
   public void setIsDriverAcceptence(Boolean isDriverAcceptence) {
       this.isDriverAcceptence = isDriverAcceptence;
   }

   /**
    * @return the isDriverAcceptence
    */
   public Boolean getIsDriverAcceptence() {
       return isDriverAcceptence;
   }

   /**
    * @param waitingTime the active to set
    */
   public void setWaitingTime(Integer waitingTime) {
       this.waitingTime = waitingTime;
   }

   /**
    * @return the waitingTime
    */
   public Integer getWaitingTime() {
       return waitingTime;
   }

   /**
    * @param retryTripCount the active to set
    */
   public void setRetryTripCount(Integer retryTripCount) {
       this.retryTripCount = retryTripCount;
   }

   /**
    * @return the retryTripCount
    */
   public Integer getRetryTripCount() {
       return retryTripCount;
   }

   public double getBookingAmount() {
       return bookingAmount;
   }

   public void setBookingAmount(double bookingAmount) {
       this.bookingAmount = bookingAmount;
   }

   public String getBookingMode() {
       return bookingMode;
   }

   public void setBookingMode(String bookingMode) {
       this.bookingMode = bookingMode;
   }

   public String getReferralCode() {
       return referralCode;
   }

   public void setReferralCode(String referralCode) {
       this.referralCode = referralCode;
   }

	public String getVehicleType() {
		return vehicleType;
	}

	public void setVehicleType(String vehicleType) {
		this.vehicleType = vehicleType;
	}

	/**
	 * @return the tripReservationId
	 */
	public Long getTripReservationId() {
		return TripReservationId;
	}

	/**
	 * @param tripReservationId the tripReservationId to set
	 */
	public void setTripReservationId(Long tripReservationId) {
		TripReservationId = tripReservationId;
	}

	/**
	 * @return the driverId
	 */
	public Long getDriverId() {
		return driverId;
	}

	/**
	 * @param driverId the driverId to set
	 */
	public void setDriverId(Long driverId) {
		this.driverId = driverId;
	}

	/**
	 * @return the selfDriverAccountName
	 */
	public String getSelfDriverAccountName() {
		return selfDriverAccountName;
	}

	/**
	 * @param selfDriverAccountName the selfDriverAccountName to set
	 */
	public void setSelfDriverAccountName(String selfDriverAccountName) {
		this.selfDriverAccountName = selfDriverAccountName;
	}

	/**
	 * @return the sourceAddress
	 */
	public String getSourceAddress() {
		return sourceAddress;
	}

	/**
	 * @param sourceAddress the sourceAddress to set
	 */
	public void setSourceAddress(String sourceAddress) {
		this.sourceAddress = sourceAddress;
	}

	/**
	 * @return the destinationAddress
	 */
	public String getDestinationAddress() {
		return destinationAddress;
	}

	/**
	 * @param destinationAddress the destinationAddress to set
	 */
	public void setDestinationAddress(String destinationAddress) {
		this.destinationAddress = destinationAddress;
	}

	/**
	 * @return the utcTravelDateTime
	 */
	public String getUtcTravelDateTime() {
		return utcTravelDateTime;
	}

	/**
	 * @param utcTravelDateTime the utcTravelDateTime to set
	 */
	public void setUtcTravelDateTime(String utcTravelDateTime) {
		this.utcTravelDateTime = utcTravelDateTime;
	}
}



