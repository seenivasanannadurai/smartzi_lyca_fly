package com.smartzi.dto.response;


public class TripFareResponse {
    private Double ratePerMileCap;
    private Double ratePerMile;
    private String vehicleType;
    private Long vehicleTypeId;
    private String description;

    public String getVehicleTypeName() {
        return vehicleTypeName;
    }

    public void setVehicleTypeName(String vehicleTypeName) {
        this.vehicleTypeName = vehicleTypeName;
    }

    private String vehicleTypeName;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    public Long getVehicleTypeId() {
        return vehicleTypeId;
    }

    public void setVehicleTypeId(Long vehicleTypeId) {
        this.vehicleTypeId = vehicleTypeId;
    }


    public Double getRatePerMileCap() {
        return ratePerMileCap;
    }

    public void setRatePerMileCap(Double ratePerMileCap) {
        this.ratePerMileCap = ratePerMileCap;
    }

    public Double getRatePerMile() {
        return ratePerMile;
    }

    public void setRatePerMile(Double ratePerMile) {
        this.ratePerMile = ratePerMile;
    }

    public String getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }


}
