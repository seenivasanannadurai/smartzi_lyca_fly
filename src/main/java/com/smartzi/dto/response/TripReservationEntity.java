package com.smartzi.dto.response;

/**
 * Entity to hold trip reservation details
 */
public class TripReservationEntity {

	private String driverName;

	private Long tripReservationId;

	private Long tripRequestId;

	private String sourceAddress;

	private Double sourceLat;

	private Double sourceLong;

	private String sourceTimeZone;

	private String destinationAddress;

	private Double destinationLat;

	private Double destinationLong;

	private String tripReservationDateTime;

	private String travelDateTime;

	private Integer tripReservationStatus;

	private Integer tripReservationType;

	private Long driverId;

	private Double ratePerMile;

	private Double minRate;

	private Integer paymentMode;

	private Double estimatedFare;

	private String bookingId;

	private String utcTravelDateTime;

	private String utcTravelEndDateTime;

	private Long paymentId;

	private Long tripId;

	private Integer tripStatus;

	private Double distance;

	private String estimatedTime;

	private String firstName;

	private String lastName;

	private Integer updatedPaymentMode;

	private Integer tripStatusUpdatedBy;

	private Integer driverRating;

	private Integer tripReservationStatusHistory;

	private Integer tripReservationStatusUpdatedBy;

	private Long customerId;

	private Integer pickCustomerEnableTime;

	private Integer rideReadyEnableDistance;

	private String mobileNo;

	private String drivermobileNo;

	private String tripStartTime;

	private String tripEndTime;

	private String vehicleType;

	private Long count;

	/**
	 * @return the tripEndTime
	 */
	public String getTripEndTime() {
		return tripEndTime;
	}

	public void setTripEndTime(String tripEndTime) {
		this.tripEndTime = tripEndTime;
	}

	/**
	 * @return the tripStartTime
	 */
	public String getTripStartTime() {
		return tripStartTime;
	}

	public void setTripStartTime(String tripStartTime) {
		this.tripStartTime = tripStartTime;
	}

	public String getdrivermobileNo() {
		return drivermobileNo;
	}

	public void setdrivermobileNo(String drivermobileNo) {
		this.drivermobileNo = drivermobileNo;
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	/**
	 * @return the pickCustomerEnableTime
	 */
	public Integer getPickCustomerEnableTime() {
		return pickCustomerEnableTime;
	}

	/**
	 * @param pickCustomerEnableTime
	 *            the pickCustomerEnableTime to set
	 */
	public void setPickCustomerEnableTime(Integer pickCustomerEnableTime) {
		this.pickCustomerEnableTime = pickCustomerEnableTime;
	}

	/**
	 * @return the rideReadyEnableDistance
	 */
	public Integer getRideReadyEnableDistance() {
		return rideReadyEnableDistance;
	}

	/**
	 * @param rideReadyEnableDistance
	 *            the rideReadyEnableDistance to set
	 */
	public void setRideReadyEnableDistance(Integer rideReadyEnableDistance) {
		this.rideReadyEnableDistance = rideReadyEnableDistance;
	}

	/**
	 * @return the tripReservationId
	 */
	public Long getTripReservationId() {
		return tripReservationId;
	}

	/**
	 * @param tripReservationId
	 *            the tripReservationId to set
	 */
	public void setTripReservationId(Long tripReservationId) {
		this.tripReservationId = tripReservationId;
	}

	/**
	 * @return the tripRequestId
	 */
	public Long getTripRequestId() {
		return tripRequestId;
	}

	/**
	 * @param tripRequestId
	 *            the tripRequestId to set
	 */
	public void setTripRequestId(Long tripRequestId) {
		this.tripRequestId = tripRequestId;
	}

	/**
	 * @return the sourceAddress
	 */
	public String getSourceAddress() {
		return sourceAddress;
	}

	/**
	 * @param sourceAddress
	 *            the sourceAddress to set
	 */
	public void setSourceAddress(String sourceAddress) {
		this.sourceAddress = sourceAddress;
	}

	/**
	 * @return the sourceLat
	 */
	public Double getSourceLat() {
		return sourceLat;
	}

	/**
	 * @param sourceLat
	 *            the sourceLat to set
	 */
	public void setSourceLat(Double sourceLat) {
		this.sourceLat = sourceLat;
	}

	/**
	 * @return the sourceLong
	 */
	public Double getSourceLong() {
		return sourceLong;
	}

	/**
	 * @param sourceLong
	 *            the sourceLong to set
	 */
	public void setSourceLong(Double sourceLong) {
		this.sourceLong = sourceLong;
	}

	/**
	 * @return the sourceTimeZone
	 */
	public String getSourceTimeZone() {
		return sourceTimeZone;
	}

	/**
	 * @param sourceTimeZone
	 *            the sourceTimeZone to set
	 */
	public void setSourceTimeZone(String sourceTimeZone) {
		this.sourceTimeZone = sourceTimeZone;
	}

	/**
	 * @return the destinationAddress
	 */
	public String getDestinationAddress() {
		return destinationAddress;
	}

	/**
	 * @param destinationAddress
	 *            the destinationAddress to set
	 */
	public void setDestinationAddress(String destinationAddress) {
		this.destinationAddress = destinationAddress;
	}

	/**
	 * @return the destinationLat
	 */
	public Double getDestinationLat() {
		return destinationLat;
	}

	/**
	 * @param destinationLat
	 *            the destinationLat to set
	 */
	public void setDestinationLat(Double destinationLat) {
		this.destinationLat = destinationLat;
	}

	/**
	 * @return the destinationLong
	 */
	public Double getDestinationLong() {
		return destinationLong;
	}

	/**
	 * @param destinationLong
	 *            the destinationLong to set
	 */
	public void setDestinationLong(Double destinationLong) {
		this.destinationLong = destinationLong;
	}

	/**
	 * @return the tripReservationDateTime
	 */
	public String getTripReservationDateTime() {
		return tripReservationDateTime;
	}

	/**
	 * @param tripReservationDateTime
	 *            the tripReservationDateTime to set
	 */
	public void setTripReservationDateTime(String tripReservationDateTime) {
		this.tripReservationDateTime = tripReservationDateTime;
	}

	/**
	 * @return the travelDateTime
	 */
	public String getTravelDateTime() {
		return travelDateTime;
	}

	/**
	 * @param travelDateTime
	 *            the travelDateTime to set
	 */
	public void setTravelDateTime(String travelDateTime) {
		this.travelDateTime = travelDateTime;
	}

	/**
	 * @return the tripReservationStatus
	 */
	public Integer getTripReservationStatus() {
		return tripReservationStatus;
	}

	/**
	 * @param tripReservationStatus
	 *            the tripReservationStatus to set
	 */
	public void setTripReservationStatus(Integer tripReservationStatus) {
		this.tripReservationStatus = tripReservationStatus;
	}

	/**
	 * @return the tripReservationType
	 */
	public Integer getTripReservationType() {
		return tripReservationType;
	}

	/**
	 * @param tripReservationType
	 *            the tripReservationType to set
	 */
	public void setTripReservationType(Integer tripReservationType) {
		this.tripReservationType = tripReservationType;
	}

	/**
	 * @return the driverId
	 */
	public Long getDriverId() {
		return driverId;
	}

	/**
	 * @param driverId
	 *            the driverId to set
	 */
	public void setDriverId(Long driverId) {
		this.driverId = driverId;
	}

	/**
	 * @return the ratePerMile
	 */
	public Double getRatePerMile() {
		return ratePerMile;
	}

	/**
	 * @param ratePerMile
	 *            the ratePerMile to set
	 */
	public void setRatePerMile(Double ratePerMile) {
		this.ratePerMile = ratePerMile;
	}

	/**
	 * @return the minRate
	 */
	public Double getMinRate() {
		return minRate;
	}

	/**
	 * @param minRate
	 *            the minRate to set
	 */
	public void setMinRate(Double minRate) {
		this.minRate = minRate;
	}

	/**
	 * @return the paymentMode
	 */
	public Integer getPaymentMode() {
		return paymentMode;
	}

	/**
	 * @param paymentMode
	 *            the paymentMode to set
	 */
	public void setPaymentMode(Integer paymentMode) {
		this.paymentMode = paymentMode;
	}

	/**
	 * @return the estimatedFare
	 */
	public Double getEstimatedFare() {
		return estimatedFare;
	}

	/**
	 * @param estimatedFare
	 *            the estimatedFare to set
	 */
	public void setEstimatedFare(Double estimatedFare) {
		this.estimatedFare = estimatedFare;
	}

	/**
	 * @return the bookingId
	 */
	public String getBookingId() {
		return bookingId;
	}

	/**
	 * @param bookingId
	 *            the bookingId to set
	 */
	public void setBookingId(String bookingId) {
		this.bookingId = bookingId;
	}

	/**
	 * @return the utcTravelDateTime
	 */
	public String getUtcTravelDateTime() {
		return utcTravelDateTime;
	}

	/**
	 * @param utcTravelDateTime
	 *            the utcTravelDateTime to set
	 */
	public void setUtcTravelDateTime(String utcTravelDateTime) {
		this.utcTravelDateTime = utcTravelDateTime;
	}

	/**
	 * @return the utcTravelEndDateTime
	 */
	public String getUtcTravelEndDateTime() {
		return utcTravelEndDateTime;
	}

	/**
	 * @param utcTravelEndDateTime
	 *            the utcTravelEndDateTime to set
	 */
	public void setUtcTravelEndDateTime(String utcTravelEndDateTime) {
		this.utcTravelEndDateTime = utcTravelEndDateTime;
	}

	/**
	 * @return the paymentId
	 */
	public Long getPaymentId() {
		return paymentId;
	}

	/**
	 * @param paymentId
	 *            the paymentId to set
	 */
	public void setPaymentId(Long paymentId) {
		this.paymentId = paymentId;
	}

	/**
	 * @return the tripStatus
	 */
	public Integer getTripStatus() {
		return tripStatus;
	}

	/**
	 * @param tripStatus
	 *            the tripStatus to set
	 */
	public void setTripStatus(Integer tripStatus) {
		this.tripStatus = tripStatus;
	}

	/**
	 * @return the distance
	 */
	public Double getDistance() {
		return distance;
	}

	/**
	 * @param distance
	 *            the distance to set
	 */
	public void setDistance(Double distance) {
		this.distance = distance;
	}

	/**
	 * @return the estimatedTime
	 */
	public String getEstimatedTime() {
		return estimatedTime;
	}

	/**
	 * @param estimatedTime
	 *            the estimatedTime to set
	 */
	public void setEstimatedTime(String estimatedTime) {
		this.estimatedTime = estimatedTime;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName
	 *            the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName
	 *            the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return the updatedPaymentMode
	 */
	public Integer getUpdatedPaymentMode() {
		return updatedPaymentMode;
	}

	/**
	 * @param updatedPaymentMode
	 *            the updatedPaymentMode to set
	 */
	public void setUpdatedPaymentMode(Integer updatedPaymentMode) {
		this.updatedPaymentMode = updatedPaymentMode;
	}

	/**
	 * @return the tripStatusUpdatedBy
	 */
	public Integer getTripStatusUpdatedBy() {
		return tripStatusUpdatedBy;
	}

	/**
	 * @param tripStatusUpdatedBy
	 *            the tripStatusUpdatedBy to set
	 */
	public void setTripStatusUpdatedBy(Integer tripStatusUpdatedBy) {
		this.tripStatusUpdatedBy = tripStatusUpdatedBy;
	}

	/**
	 * @return the driverRating
	 */
	public Integer getDriverRating() {
		return driverRating;
	}

	/**
	 * @param driverRating
	 *            the driverRating to set
	 */
	public void setDriverRating(Integer driverRating) {
		this.driverRating = driverRating;
	}

	/**
	 * @return the tripReservationStatusHistory
	 */
	public Integer getTripReservationStatusHistory() {
		return tripReservationStatusHistory;
	}

	/**
	 * @param tripReservationStatusHistory
	 *            the tripReservationStatusHistory to set
	 */
	public void setTripReservationStatusHistory(Integer tripReservationStatusHistory) {
		this.tripReservationStatusHistory = tripReservationStatusHistory;
	}

	/**
	 * @return the tripReservationStatusUpdatedBy
	 */
	public Integer getTripReservationStatusUpdatedBy() {
		return tripReservationStatusUpdatedBy;
	}

	/**
	 * @param tripReservationStatusUpdatedBy
	 *            the tripReservationStatusUpdatedBy to set
	 */
	public void setTripReservationStatusUpdatedBy(Integer tripReservationStatusUpdatedBy) {
		this.tripReservationStatusUpdatedBy = tripReservationStatusUpdatedBy;
	}

	/**
	 * @return the tripId
	 */
	public Long getTripId() {
		return tripId;
	}

	/**
	 * @param tripId
	 *            the tripId to set
	 */
	public void setTripId(Long tripId) {
		this.tripId = tripId;
	}

	/**
	 * @return the customerId
	 */
	public Long getCustomerId() {
		return customerId;
	}

	/**
	 * @param customerId
	 *            the customerId to set
	 */
	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public String getDriverName() {
		return driverName;
	}

	public void setDriverName(String driverName) {
		this.driverName = driverName;
	}

	public String getVehicleType() {
		return vehicleType;
	}

	public void setVehicleType(String vehicleType) {
		this.vehicleType = vehicleType;
	}

	public Long getCount() {
		return count;
	}

	public void setCount(Long count) {
		this.count = count;
	}

}
