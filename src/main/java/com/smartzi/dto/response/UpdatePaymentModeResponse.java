package com.smartzi.dto.response;


public class UpdatePaymentModeResponse {
    /**
     *
     */
    private Double tripFare;
    /**
     *
     */
    private Integer paymentMode;
    /**
     *
     */
    private Long creditCardNumber;
    /**
     *
     */
    private Long tripReservationId;

    /**
     *
     */
    private String airportTrip;
    /**
     *
     */
    private String flightNumber;
    /**
     *
     */
    private String flightDatetime;
    /**
     *
     */
    private Integer passangerCount;
    /**
     *
     */
    private Integer lauggageCount;
    /**
     *
     */
    private String isAssign;


    /**
     *
     */

    private Long driverId;
    /**
     *
     */
    private boolean controllerBookingTrip;

    public String getBookingId() {
        return bookingId;
    }

    public void setBookingId(String bookingId) {
        this.bookingId = bookingId;
    }

    /**
     *
     */
    private String bookingId;

    /**
     * @return the airportTrip
     */
    public String getAirportTrip() {
        return airportTrip;
    }

    /**
     * @param airportTrip the airportTrip to set
     */
    public void setAirportTrip(String airportTrip) {
        this.airportTrip = airportTrip;
    }


    /**
     * @return the flightNumber
     */
    public String getFlightNumber() {
        return flightNumber;
    }

    /**
     * @param flightNumber the flightNumber to set
     */
    public void setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
    }

    /**
     * @return the passangerCount
     */
    public Integer getPassangerCount() {
        return passangerCount;
    }

    /**
     * @param passangerCount the passangerCount to set
     */
    public void setPassangerCount(Integer passangerCount) {
        this.passangerCount = passangerCount;
    }

    /**
     * @return the lauggageCount
     */
    public Integer getLauggageCount() {
        return lauggageCount;
    }

    /**
     * @param lauggageCount the lauggageCount to set
     */
    public void setLauggageCount(Integer lauggageCount) {
        this.lauggageCount = lauggageCount;
    }


    /**
     * @return the flightDatetime
     */
    public String getFlightDatetime() {
        return flightDatetime;
    }

    /**
     * @param flightDatetime the flightDatetime to set
     */
    public void setFlightDatetime(String flightDatetime) {
        this.flightDatetime = flightDatetime;
    }


    /**
     * @return the tripFare
     */
    public Double getTripFare() {
        return tripFare;
    }

    /**
     * @param tripFare the tripFare to set
     */
    public void setTripFare(Double tripFare) {
        this.tripFare = tripFare;
    }

    /**
     * @return the paymentMode
     */
    public Integer getPaymentMode() {
        return paymentMode;
    }

    /**
     * @param paymentMode the paymentMode to set
     */
    public void setPaymentMode(Integer paymentMode) {
        this.paymentMode = paymentMode;
    }

    /**
     * @return the creditCardNumber
     */
    public Long getCreditCardNumber() {
        return creditCardNumber;
    }

    /**
     * @param creditCardNumber the creditCardNumber to set
     */
    public void setCreditCardNumber(Long creditCardNumber) {
        this.creditCardNumber = creditCardNumber;
    }

    /**
     * @return the tripReservationId
     */
    public Long getTripReservationId() {
        return tripReservationId;
    }

    /**
     * @param tripReservationId the tripReservationId to set
     */
    public void setTripReservationId(Long tripReservationId) {
        this.tripReservationId = tripReservationId;
    }

    public String getIsAssign() {
        return isAssign;
    }

    public void setIsAssign(String isAssign) {
        this.isAssign = isAssign;
    }

    public Long getDriverId() {
        return driverId;
    }

    public void setDriverId(Long driverId) {
        this.driverId = driverId;
    }

    public boolean isControllerBookingTrip() {
        return controllerBookingTrip;
    }

    public void setControllerBookingTrip(boolean controllerBookingTrip) {
        this.controllerBookingTrip = controllerBookingTrip;
    }


}
