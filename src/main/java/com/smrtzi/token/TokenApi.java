package com.smrtzi.token;



// TODO: Auto-generated Javadoc
/**
 * The Interface TokenApi.
 */
public interface TokenApi {
    
    /**
     * Gets the api token.
     *
     * @return the api token
     */
    public String getApiToken();
    
    /**
     * Put api token.
     *
     * @return the string
     */
    public String putApiToken();
}
