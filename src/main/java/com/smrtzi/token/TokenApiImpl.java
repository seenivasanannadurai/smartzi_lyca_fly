package com.smrtzi.token;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;



// TODO: Auto-generated Javadoc
/**
 * The Class TokenApiImpl.
 */
@Component
@CacheConfig(cacheNames={"tokenCache"})
public class TokenApiImpl implements TokenApi {
	
	/** The token generator. */
	@Autowired
    TokenGenerator tokenGenerator;
    
    /* (non-Javadoc)
     * @see com.smartzi.here.token.TokenApi#getApiToken()
     */
    @Override
    @Cacheable(value = "tokenCache")
    public String getApiToken() {
        return tokenGenerator.generateToken();
    }
   
    /* (non-Javadoc)
     * @see com.smartzi.here.token.TokenApi#putApiToken()
     */
    @Scheduled(fixedRate = 950000)
    @CachePut(value = "tokenCache") 	
    public String putApiToken(){
     return tokenGenerator.generateToken();
    }
    
    
}
