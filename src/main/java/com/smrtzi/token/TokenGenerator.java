package com.smrtzi.token;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.smartzi.dto.request.AdminLoginRequest;
import com.smartzi.dto.response.AdminLoginResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import java.io.IOException;

// TODO: Auto-generated Javadoc
/**
 * The Class TokenGenerator.
 */
@Component
public class TokenGenerator {

	/** The rest template. */
	@Autowired
	RestTemplate restTemplate;

    /** The user name. */
    @Value("${here.username}")
	private String userName;

    /** The pass word. */
    @Value("${here.password}")
	private String passWord;

    /** The base url. */
    @Value("${smartzi.base.url}")
   	private String baseUrl;
    
    /** The trip request url. */
    @Value("${smartzi.trip.request}")
   	private String tripRequestUrl;
    
    /** The login url. */
    @Value("${smartzi.admin.login}")
   	private String loginUrl;
    
	/**
	 * Generate token.
	 *
	 * @return the string
	 */
	public String generateToken() {
		AdminLoginResponse adminloginResponse = new AdminLoginResponse();
		AdminLoginRequest adminLoginRequest = new AdminLoginRequest();
		adminLoginRequest.setEmailId(userName);
		adminLoginRequest.setPassword(passWord);
		String adminloginResponse1 = restTemplate.postForEntity(baseUrl + loginUrl, adminLoginRequest, String.class).getBody();
		System.out.println("accesstoken Res**********" + adminloginResponse1);

		JsonNode actualObj = null;
		try {
			actualObj = new ObjectMapper().readTree(adminloginResponse1);
			adminloginResponse = new ObjectMapper().readValue(actualObj.get("data").toString(),
					AdminLoginResponse.class);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return adminloginResponse.getoAuthResponse().getAccess_token();

	}
}
